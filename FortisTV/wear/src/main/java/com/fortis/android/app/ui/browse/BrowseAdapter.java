package com.fortis.android.app.ui.browse;

import android.content.Context;
import android.support.wearable.view.GridPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fortis.android.app.R;
import com.fortis.android.app.ui.comment.CommentActivity;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.data.model.User;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

public class BrowseAdapter extends GridPagerAdapter {

    private Context mContext;
    private List<Feed> mFeeds;

    public BrowseAdapter(Context context) {
        mContext = context;
        mFeeds = Collections.emptyList();
    }

    public void setFeeds(List<Feed> feeds) {
        mFeeds = feeds;
    }

    @Override
    public int getRowCount() {
        return mFeeds.size();
    }

    @Override
    public int getColumnCount(int i) {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup viewGroup, int i, int i1) {
        Feed feed = mFeeds.get(i);

        if (i1 == 0) {
            FrameLayout view = (FrameLayout) LayoutInflater.from(mContext).inflate(R.layout.item_shot, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.bind(feed);
            view.setTag(viewHolder);
            viewGroup.addView(view);
            return viewHolder.frameLayout;
        } else if (i1 == 1) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_user, viewGroup, false);
            UserViewHolder viewHolder = new UserViewHolder(view);

            User user = new User();
            user.login = "opage";
            user.fullName = "Olivier Page";
            user.avatar = "https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-simple-512.png";

            viewHolder.bind(user);
            view.setTag(viewHolder);
            viewGroup.addView(view);
            return viewHolder.frameLayout;
        } else if (i1 == 2) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_detail, viewGroup, false);
            DetailViewHolder viewHolder = new DetailViewHolder(view);
            viewHolder.bind("10340", R.drawable.ic_favorite_accent_48dp);
            view.setTag(viewHolder);
            viewGroup.addView(view);
            return viewHolder.frameLayout;
        } else if (i1 == 3) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_detail, viewGroup, false);
            DetailViewHolder viewHolder = new DetailViewHolder(view);
            viewHolder.bind("123456", R.drawable.ic_visibility_accent_48dp);
            view.setTag(viewHolder);
            viewGroup.addView(view);
            return viewHolder.frameLayout;
        }

        return null;
    }

    @Override
    public void destroyItem(ViewGroup viewGroup, int i, int i1, Object o) {
        viewGroup.removeView((View) o);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view.equals(o);
    }

    class ViewHolder {
        ImageView imageView;

        FrameLayout frameLayout;

        public ViewHolder(FrameLayout frameLayout) {
            this.frameLayout = frameLayout;
            imageView = frameLayout.findViewById(R.id.image_shot);
        }

        void bind(final Feed feed) {
            Glide.with(mContext)
                    .load(feed.image)
                    .into(imageView);
            frameLayout.setOnClickListener(v -> mContext.startActivity(CommentActivity.newIntent(mContext, feed)));
        }
    }

    class UserViewHolder {
        TextView titleView;
        ImageView userImage;
        TextView userName;

        View frameLayout;

        public UserViewHolder(View frameLayout) {
            this.frameLayout = frameLayout;
            titleView = frameLayout.findViewById(R.id.text_title);
            userImage = frameLayout.findViewById(R.id.image_avatar);
            userName = frameLayout.findViewById(R.id.text_user);
        }

        void bind(final User user) {
            titleView.setText(user.fullName);
            userName.setText(user.login);
            Glide.with(mContext).load(user.avatar).into(userImage);
        }
    }

    class DetailViewHolder {
        TextView countText;
        View frameLayout;

        public DetailViewHolder(View frameLayout) {
            this.frameLayout = frameLayout;
            countText = frameLayout.findViewById(R.id.text_count);
        }

        void bind(String text, int iconRes) {
            countText.setText(formatString(text));
            countText.setCompoundDrawablesWithIntrinsicBounds(0, iconRes, 0, 0);
        }
    }

    private String formatString(String number) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        int n = Integer.valueOf(number);
        return formatter.format(n);
    }
}
