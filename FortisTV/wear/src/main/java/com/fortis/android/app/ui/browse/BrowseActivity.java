package com.fortis.android.app.ui.browse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.widget.ProgressBar;

import com.fortis.android.app.R;
import com.fortis.android.app.bus.event.FeedsEvent;
import com.fortis.android.app.databinding.ActivityBrowseBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.viewmodel.BrowseViewModel;
import com.fortis.android.core.bus.BusProvider;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

public class BrowseActivity extends BaseActivity<ActivityBrowseBinding, BrowseViewModel> {

    // ----- Fields
    @BindView(R.id.pager_shots)
    GridViewPager mShotsPager;

    @BindView(R.id.page_indicator)
    DotsPageIndicator mPageIndicator;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    private BrowseAdapter mBrowseAdapter;

    // ----- Override methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_browse, savedInstanceState);
        BusProvider.register(this);

        mBrowseAdapter = new BrowseAdapter(this);
        mShotsPager.setAdapter(mBrowseAdapter);
        mPageIndicator.setPager(mShotsPager);
        mPageIndicator.setDotColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mPageIndicator.setDotColorSelected(ContextCompat.getColor(this, R.color.colorAccent));
        mPageIndicator.setDotRadius(4);
        viewModel.getFeeds(10, 0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showFeeds(FeedsEvent event) {
        mBrowseAdapter.setFeeds(event.feeds);
        mBrowseAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.layout_error)
    public void onErrorLayoutClick() {
        viewModel.getFeeds(10, 0);
    }
}
