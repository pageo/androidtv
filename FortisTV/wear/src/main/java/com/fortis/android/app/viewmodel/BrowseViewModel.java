package com.fortis.android.app.viewmodel;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.fortis.android.app.R;
import com.fortis.android.app.bus.event.FeedsEvent;
import com.fortis.android.core.bus.BusProvider;
import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;
import com.tumblr.remember.Remember;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class BrowseViewModel extends BaseViewModel {

    // ----- Fields
    private final DataManager dataManager;
    private final Resources resources;
    private Disposable disposable;
    private boolean isEmpty = true;
    private boolean errorVisible = true;
    private boolean progressVisible = false;

    // ----- Constructors
    @Inject
    public BrowseViewModel(DataManager dataManager, Resources resources) {
        this.dataManager = dataManager;
        this.resources = resources;
    }

    // ----- Public  methods
    public Drawable getImageError() {
        return this.isEmpty ? this.resources.getDrawable(R.drawable.ic_empty_glass_gray_48dp)
                : this.resources.getDrawable(R.drawable.ic_sentiment_very_dissatisfied_gray_48dp);
    }

    public String getTextError() {
        return this.isEmpty ? this.resources.getString(R.string.text_no_shots)
                : this.resources.getString(R.string.text_error_loading_shots);
    }

    public void setEmpty(boolean empty) {
        this.isEmpty = empty;
        notifyChange();
    }

    public void setErrorVisible(boolean errorVisible) {
        this.errorVisible = errorVisible;
        notifyChange();
    }
    public boolean getErrorVisible() {
        return errorVisible;
    }

    public void setProgressVisible(boolean progressVisible) {
        this.progressVisible = progressVisible;
        notifyChange();
    }

    public boolean getProgressVisible() {
        return this.progressVisible;
    }

    public void getFeeds(int i, int i1) {
        showMessageLayout(false);
        setProgressVisible(true);

        Remember.putString("Token", "aWQ6MztjcmVhdGlvbkRhdGU6MTEvMDgvMjAxNyAxNjozMjozNg==");

        disposable = dataManager.getFeeds()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(feeds -> {
                    setProgressVisible(false);
                    if (!feeds.isEmpty())
                        BusProvider.post(new FeedsEvent(feeds));
                    else
                        showEmpty();
                }, throwable -> {
                    Timber.e(throwable, "There was an error retrieving the feeds");
                    setProgressVisible(false);
                    showError();
                });
    }

    private void showError() {
        setEmpty(false);
    }

    private void showEmpty() {
        setEmpty(true);
    }

    private void showMessageLayout(boolean show) {
        setErrorVisible(show);
    }
}
