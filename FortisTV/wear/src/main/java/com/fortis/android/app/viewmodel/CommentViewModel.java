package com.fortis.android.app.viewmodel;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.fortis.android.app.R;
import com.fortis.android.app.bus.event.CommentsEvent;
import com.fortis.android.core.bus.BusProvider;
import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.Comment;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class CommentViewModel extends BaseViewModel {

    // ----- Fields
    private final DataManager dataManager;
    private final Resources resources;
    private List<Comment> mComments;
    private Disposable disposable;
    private boolean isEmpty = false;
    private boolean progressVisible = false;
    private boolean errorVisible = false;
    private boolean pagerVisible = false;
    private boolean footerLayoutVisible = false;

    @Inject
    public CommentViewModel(DataManager dataManager, Resources resources) {
        this.dataManager = dataManager;
        this.resources = resources;
    }

    // ----- Public  methods
    public Drawable getImageError() {
        return this.isEmpty ? this.resources.getDrawable(R.drawable.ic_empty_glass_gray_48dp)
                : this.resources.getDrawable(R.drawable.ic_sentiment_very_dissatisfied_gray_48dp);
    }

    public String getTextError() {
        return this.isEmpty ? this.resources.getString(R.string.text_no_recent_comments)
                : this.resources.getString(R.string.text_error_loading_comments);
    }

    public String getActionText() {
        return this.isEmpty ? this.resources.getString(R.string.text_check_again)
                : this.resources.getString(R.string.text_reload);
    }

    public void setEmpty(boolean empty) {
        this.isEmpty = empty;
        notifyChange();
    }

    public void setPagerVisible(boolean pagerVisible) {
        this.pagerVisible = pagerVisible;
        notifyChange();
    }
    public boolean getPagerVisible() {
        return this.pagerVisible;
    }

    public void setFooterLayoutVisible(boolean footerLayoutVisible) {
        this.footerLayoutVisible = footerLayoutVisible;
        notifyChange();
    }
    public boolean getFooterLayoutVisible() {
        return footerLayoutVisible;
    }

    public void setProgressVisible(boolean progressVisible) {
        this.progressVisible = progressVisible;
    }
    public boolean getProgressVisible() {
        return progressVisible;
    }

    public void setErrorVisible(boolean errorVisible) {
        this.errorVisible = errorVisible;
        notifyChange();
    }
    public boolean getErrorVisible() {
        return errorVisible;
    }

    public void getComments(int id, int perPage, int page) {
        setProgressVisible(true);

        Single<List<Comment>> single;
        if (mComments == null)
            single = dataManager.getComments(id, perPage, page);
        else
            single = Single.just(mComments);

        disposable = single
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(comments -> {
                    mComments = comments;
                    setProgressVisible(false);
                    if (comments.isEmpty())
                        showEmptyComments();
                    else
                        BusProvider.post(new CommentsEvent(comments));
                }, throwable -> {
                    Timber.e(throwable, "There was an error retrieving the comments");
                    setProgressVisible(false);
                    showError();
                });
    }

    private void showError() {
        setEmpty(false);
        setUIErrorState(true);
    }

    private void showEmptyComments() {
        setEmpty(true);
        setUIErrorState(true);
    }

    private void setUIErrorState(boolean isError) {
        setFooterLayoutVisible(isError);
        setPagerVisible(isError);
        setErrorVisible(isError);
    }
}
