package com.fortis.android.app.bus.event;

import com.fortis.android.core.bus.IBusEvent;
import com.fortis.android.core.data.model.Comment;

import java.util.List;

public class CommentsEvent implements IBusEvent {
    public final List<Comment> comments;

    public CommentsEvent(List<Comment> comments) {
        this.comments = comments;
    }
}
