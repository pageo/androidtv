package com.fortis.android.app.injection.components;

import com.fortis.android.app.ui.browse.BrowseActivity;
import com.fortis.android.app.ui.comment.CommentActivity;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.modules.ActivityModule;
import com.fortis.android.core.injection.modules.ProviderModule;
import com.fortis.android.core.injection.scopes.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ProviderModule.class})
public interface ActivityComponent {
    void inject(BrowseActivity browseActivity);
    void inject(CommentActivity commentActivity);
}
