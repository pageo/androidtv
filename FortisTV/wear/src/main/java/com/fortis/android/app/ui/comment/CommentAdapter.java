package com.fortis.android.app.ui.comment;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fortis.android.app.R;
import com.fortis.android.core.data.model.Comment;

import java.util.Collections;
import java.util.List;

public class CommentAdapter extends PagerAdapter {

    private Context mContext;
    private List<Comment> mComments;

    public CommentAdapter(Context context) {
        mContext = context;
        mComments = Collections.emptyList();
    }

    public void setComments(List<Comment> comments) {
        mComments = comments;
    }

    @Override
    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Comment comment = mComments.get(i);

        ViewHolder viewHolder;
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_comment, viewGroup, false);
        viewHolder = new ViewHolder(view);
        viewHolder.bind(comment);
        view.setTag(viewHolder);
        viewGroup.addView(view);
        return viewHolder.frameLayout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }


    @Override
    public int getCount() {
        return mComments.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view.equals(o);
    }

    class ViewHolder {
        TextView textView;
        ImageView imageView;
        TextView userText;

        View frameLayout;

        public ViewHolder(View frameLayout) {
            this.frameLayout = frameLayout;
            textView = frameLayout.findViewById(R.id.text_comment);
            userText = frameLayout.findViewById(R.id.text_user);
            imageView = frameLayout.findViewById(R.id.image_avatar);
        }

        void bind(Comment comment) {
            textView.setText(Html.fromHtml(comment.body));
            userText.setText(comment.user.login);
            Glide.with(mContext).load(comment.user.avatar).into(imageView);
        }
    }
}
