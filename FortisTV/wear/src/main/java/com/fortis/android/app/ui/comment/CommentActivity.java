package com.fortis.android.app.ui.comment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fortis.android.app.R;
import com.fortis.android.app.bus.event.CommentsEvent;
import com.fortis.android.app.databinding.ActivityCommentBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.widget.CircleIndicatorView;
import com.fortis.android.app.viewmodel.CommentViewModel;
import com.fortis.android.core.bus.BusProvider;
import com.fortis.android.core.data.model.Feed;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;

public class CommentActivity extends BaseActivity<ActivityCommentBinding, CommentViewModel> {

    public static final String EXTRA_FEED =
            "com.fortis.android.app.ui.comment.CommentActivity.EXTRA_FEED";

    @BindView(R.id.image_message)
    ImageView mErrorImage;
    @BindView(R.id.page_indicator)
    CircleIndicatorView mCircleIndicatorView;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.layout_footer)
    RelativeLayout mFooterlayout;
    @BindView(R.id.text_message)
    TextView mErrorText;
    @BindView(R.id.text_message_action)
    TextView mActionText;
    @BindView(R.id.layout_message)
    View mErrorView;
    @BindView(R.id.pager_comments)
    ViewPager mShotsPager;

    private CommentAdapter mCommentAdapter;
    private Feed mFeed;

    public static Intent newIntent(Context context, Feed feed) {
        Intent intent = new Intent(context, CommentActivity.class);
        intent.putExtra(EXTRA_FEED, Parcels.wrap(feed));
        return intent;
    }

    // ----- Override methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_comment, savedInstanceState);
        BusProvider.register(this);

        mFeed = getIntent().getParcelableExtra(EXTRA_FEED);

        if (mFeed == null)
            throw new IllegalArgumentException("CommentActivity requires a feed instance!");

        mCommentAdapter = new CommentAdapter(this);
        mShotsPager.setAdapter(mCommentAdapter);

        viewModel.getComments(mFeed.id, 10, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.unregister(this);
    }

    @OnClick(R.id.text_message_action)
    public void onTextMessageActionClick() {
        viewModel.getComments(mFeed.id, 10, 0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showComments(CommentsEvent event) {
        mCommentAdapter.setComments(event.comments);
        mCommentAdapter.notifyDataSetChanged();
        mCircleIndicatorView.attachViewPager(mShotsPager);
        mCircleIndicatorView.bringToFront();
    }
}
