package com.fortis.android.app.bus.event;

import com.fortis.android.core.bus.IBusEvent;
import com.fortis.android.core.data.model.Feed;

import java.util.List;

public class FeedsEvent implements IBusEvent {

    public final List<Feed> feeds;

    public FeedsEvent(List<Feed> feeds) {
        this.feeds = feeds;
    }
}
