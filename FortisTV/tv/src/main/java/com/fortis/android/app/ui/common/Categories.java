package com.fortis.android.app.ui.common;

public enum Categories {
    FEEDS(0),
    ANIMES(1);

    private final int value;

    Categories(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
