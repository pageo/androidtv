package com.fortis.android.app.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.content.ContentActivity;
import com.tumblr.remember.Remember;

public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Class intentClass = !TextUtils.isEmpty(Remember.getString("Token", "")) ? ContentActivity.class : ConnectActivity.class;
        startActivity(new Intent(this, intentClass));
        finish();
    }
}