package com.fortis.android.app.injection.component;

import com.fortis.android.app.injection.module.ActivityModule;
import com.fortis.android.app.ui.content.ContentActivity;
import com.fortis.android.app.ui.main.ConnectActivity;
import com.fortis.android.app.ui.search.SearchContentActivity;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.scopes.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(ConnectActivity connectActivity);
    void inject(SearchContentActivity searchContentActivity);
    void inject(ContentActivity contentActivity);
}
