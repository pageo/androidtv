package com.fortis.android.app.ui.content;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.fortis.android.app.R;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.search.SearchContentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentActivity extends BaseActivity {

    @BindView(R.id.frame_container)
    FrameLayout mFragmentContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getFragmentManager().beginTransaction()
                .replace(mFragmentContainer.getId(), ContentFragment.newInstance()).commit();
    }

    @Override
    public boolean onSearchRequested() {
        startActivity(SearchContentActivity.getStartIntent(this));
        return true;
    }

}