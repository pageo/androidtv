package com.fortis.android.app.ui.content;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.PresenterSelector;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fortis.android.app.R;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.common.CardPresenter;
import com.fortis.android.app.ui.common.Categories;
import com.fortis.android.app.ui.common.IconHeaderItemPresenter;
import com.fortis.android.app.ui.search.SearchContentActivity;
import com.fortis.android.core.data.model.Anime;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.presenter.content.ContentMvpView;
import com.fortis.android.core.presenter.content.ContentPresenter;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;

public class ContentFragment extends BrowseFragment implements ContentMvpView {

    @Inject
    ContentPresenter mContentPresenter;

    private static final int BACKGROUND_UPDATE_DELAY = 300;
    private ArrayObjectAdapter mRowsAdapter;
    private BackgroundManager mBackgroundManager;
    private DisplayMetrics mMetrics;
    private Drawable mDefaultBackground;
    private Handler mHandler;
    private Runnable mBackgroundRunnable;

    public static ContentFragment newInstance() {
        return new ContentFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseActivity) getActivity()).getFragmentComponent().inject(this);
        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        mHandler = new Handler();
        mContentPresenter.attachView(this);

        setAdapter(mRowsAdapter);
        prepareBackgroundManager();
        setupUIElements();
        setupListeners();
        getFeeds();
        getAnimes();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBackgroundRunnable != null) {
            mHandler.removeCallbacks(mBackgroundRunnable);
            mBackgroundRunnable = null;
        }
        mBackgroundManager = null;
        mContentPresenter.detachView();
    }

    @Override
    public void onStop() {
        super.onStop();
        mBackgroundManager.release();
    }

    protected void updateBackground(String uri) {
        int width = mMetrics.widthPixels;
        int height = mMetrics.heightPixels;
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .error(mDefaultBackground);

        Glide.with(getActivity())
                .asBitmap()
                .load(uri)
                .apply(options)
                .into(new SimpleTarget<Bitmap>(width, height) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        mBackgroundManager.setBitmap(resource);
                    }
                });
        if (mBackgroundRunnable != null) mHandler.removeCallbacks(mBackgroundRunnable);
    }

    private void setupUIElements() {
        setBadgeDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.banner_browse));
        setHeaderPresenterSelector(new PresenterSelector() {
            @Override
            public Presenter getPresenter(Object o) {
                return new IconHeaderItemPresenter();
            }
        });
        setHeadersState(HEADERS_ENABLED);
        setHeadersTransitionOnBackEnabled(true);
        setBrandColor(ContextCompat.getColor(getActivity(), R.color.primary));
        setSearchAffordanceColor(ContextCompat.getColor(getActivity(), R.color.accent));
    }

    private void setupListeners() {
        setOnItemViewClickedListener(mOnItemViewClickedListener);
        setOnItemViewSelectedListener(mOnItemViewSelectedListener);
        setOnSearchClickedListener(view -> startActivity(new Intent(getActivity(), SearchContentActivity.class)));
    }

    private void prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground =
                new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.primary_light));
        mBackgroundManager.setColor(ContextCompat.getColor(getActivity(), R.color.primary_light));
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void getFeeds() {
        mContentPresenter.getFeeds();
    }

    private void getAnimes() {
        mContentPresenter.getAnimes();
    }

    private void startBackgroundTimer(final URI backgroundURI) {
        if (mBackgroundRunnable != null) mHandler.removeCallbacks(mBackgroundRunnable);
        if (backgroundURI != null) mBackgroundRunnable = () -> updateBackground(backgroundURI.toString());
        mHandler.postDelayed(mBackgroundRunnable, BACKGROUND_UPDATE_DELAY);
    }

    private OnItemViewClickedListener mOnItemViewClickedListener = (itemViewHolder, item, rowViewHolder, row) ->
            Toast.makeText(getActivity(), "Hello everybody", Toast.LENGTH_SHORT).show();

    private OnItemViewSelectedListener mOnItemViewSelectedListener =
            (itemViewHolder, item, rowViewHolder, row) -> {
                if (item instanceof Feed) {
                    Feed feed = (Feed) item;
                    String backgroundUrl = feed.image;
                    if (backgroundUrl != null) startBackgroundTimer(URI.create(backgroundUrl));
                }
            };

    @Override
    public void showFeeds(List<Feed> feeds) {
        final ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new CardPresenter());
        listRowAdapter.addAll(0, feeds);
        HeaderItem header = new HeaderItem(Categories.FEEDS.getValue(), getString(R.string.header_title_feeds));
        mRowsAdapter.add(new ListRow(header, listRowAdapter));
    }

    @Override
    public void showFeedsError() {
        String errorMessage = getString(R.string.error_message_generic);
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAnimes(List<Anime> animes) {
        final ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new CardPresenter());
        listRowAdapter.addAll(0, animes);
        HeaderItem header = new HeaderItem(Categories.ANIMES.getValue(), getString(R.string.header_title_animes));
        mRowsAdapter.add(new ListRow(header, listRowAdapter));
    }

    @Override
    public void showAnimesError() {
        String errorMessage = getString(R.string.error_message_generic);
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

}