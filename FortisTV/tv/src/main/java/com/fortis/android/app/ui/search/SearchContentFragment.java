package com.fortis.android.app.ui.search;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.SearchFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.ObjectAdapter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fortis.android.app.R;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.common.CardPresenter;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.presenter.search.SearchContentMvpView;
import com.fortis.android.core.presenter.search.SearchContentPresenter;
import com.fortis.android.core.util.NetworkUtil;
import com.fortis.android.core.util.ToastPresenter;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class SearchContentFragment extends SearchFragment implements SearchContentMvpView,
        SearchFragment.SearchResultProvider {

    @Inject
    ToastPresenter mToastPresenter;

    @Inject
    SearchContentPresenter mSearchContentPresenter;

    private static final int BACKGROUND_UPDATE_DELAY = 300;
    private static final int REQUEST_SPEECH = 0x00000010;

    private ArrayObjectAdapter mResultsAdapter;
    private ArrayObjectAdapter mSearchObjectAdapter;
    private BackgroundManager mBackgroundManager;
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private Handler mHandler;
    private Runnable mBackgroundRunnable;
    private String mSearchQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).getFragmentComponent().inject(this);
        mResultsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        mHandler = new Handler();
        mSearchContentPresenter.attachView(this);
        setSearchResultProvider(this);
        setupBackgroundManager();
        setListeners();
    }

    public void onDestroy() {
        if (mBackgroundRunnable != null) {
            mHandler.removeCallbacks(mBackgroundRunnable);
            mBackgroundRunnable = null;
        }
        mBackgroundManager = null;
        mSearchContentPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        mBackgroundManager.release();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SPEECH:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        setSearchQuery(data, false);
                        break;
                    case Activity.RESULT_CANCELED:
                        Timber.i("Recognizer canceled");
                        break;
                }
                break;
        }
    }

    @Override
    public ObjectAdapter getResultsAdapter() {
        return mResultsAdapter;
    }

    @Override
    public boolean onQueryTextChange(String newQuery) {
        loadQuery(newQuery);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        loadQuery(query);
        return true;
    }

    public boolean hasResults() {
        return mResultsAdapter.size() > 0;
    }

    protected void updateBackground(String uri) {
        int width = mMetrics.widthPixels;
        int height = mMetrics.heightPixels;
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .error(mDefaultBackground);

        Glide.with(getActivity())
                .asBitmap()
                .load(uri)
                .apply(options)
                .into(new SimpleTarget<Bitmap>(width, height) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        mBackgroundManager.setBitmap(resource);
                    }
                });
        if (mBackgroundRunnable != null) mHandler.removeCallbacks(mBackgroundRunnable);
    }

    private void setupBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());
        mBackgroundManager.setColor(ContextCompat.getColor(getActivity(), R.color.primary_light));
        mDefaultBackground =
                new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.primary_light));
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void startBackgroundTimer(final URI backgroundURI) {
        if (mBackgroundRunnable != null) mHandler.removeCallbacks(mBackgroundRunnable);
        if (backgroundURI != null) mBackgroundRunnable = () -> updateBackground(backgroundURI.toString());
        mHandler.postDelayed(mBackgroundRunnable, BACKGROUND_UPDATE_DELAY);
    }

    private void setListeners() {
        setOnItemViewClickedListener(mOnItemViewClickedListener);
        setOnItemViewSelectedListener(mOnItemViewSelectedListener);
        if (!hasPermission(Manifest.permission.RECORD_AUDIO)) {
            setSpeechRecognitionCallback(() -> {
                try {
                    startActivityForResult(getRecognizerIntent(), REQUEST_SPEECH);
                } catch (ActivityNotFoundException error) {
                    Timber.e(error, "Cannot find activity for speech recognizer");
                }
            });
        }
    }

    private boolean hasPermission(final String permission) {
        final Context context = getActivity();
        return PackageManager.PERMISSION_GRANTED == context.getPackageManager().checkPermission(
                permission, context.getPackageName());
    }

    private void loadQuery(String query) {
        if ((mSearchQuery != null && !mSearchQuery.equals(query)) && !query.trim().isEmpty()
                || (!TextUtils.isEmpty(query) && !query.equals("nil"))) {
            if (NetworkUtil.isNetworkConnected(getActivity())) {
                mSearchQuery = query;
                searchFeeds(query);
            } else
                mToastPresenter.showShortToast(getString(R.string.error_message_network_needed));
        }
    }

    private void searchFeeds(String query) {
        mResultsAdapter.clear();
        HeaderItem resultsHeader = new HeaderItem(0, getString(R.string.text_search_results));
        mSearchObjectAdapter = new ArrayObjectAdapter(new CardPresenter());
        ListRow listRow = new ListRow(resultsHeader, mSearchObjectAdapter);
        mResultsAdapter.add(listRow);
        mSearchObjectAdapter.clear();
        mSearchContentPresenter.searchFeeds();
    }

    private OnItemViewClickedListener mOnItemViewClickedListener = (itemViewHolder, item, rowViewHolder, row) -> {
        // Handle item click
    };

    private OnItemViewSelectedListener mOnItemViewSelectedListener =
            (itemViewHolder, item, rowViewHolder, row) -> {
                if (item instanceof Feed) {
                    String backgroundUrl = ((Feed) item).image;
                    if (backgroundUrl != null) startBackgroundTimer(URI.create(backgroundUrl));
                }
            };

    @Override
    public void showFeeds(List<Feed> feeds) {
        mSearchObjectAdapter.addAll(0, feeds);
    }

    @Override
    public void showFeedsError() {
        // show loading error state here
        String errorMessage = getString(R.string.error_message_generic);
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}