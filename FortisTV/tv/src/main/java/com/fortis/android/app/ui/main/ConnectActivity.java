package com.fortis.android.app.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fortis.android.app.R;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.content.ContentActivity;
import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.LoginInfo;
import com.fortis.android.core.util.NetworkUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ConnectActivity extends BaseActivity {

    @BindView(R.id.button_sign_in)
    Button mSignInButton;

    @BindView(R.id.edit_text_username)
    EditText mUsernameEditText;

    @BindView(R.id.edit_text_password)
    EditText mPasswordEditText;

    @BindView(R.id.progress)
    ProgressBar mSignInProgress;

    @Inject
    DataManager mDataManager;

    private Disposable mDisposable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);

        setContentView(R.layout.activity_connect);
        ButterKnife.bind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDisposable != null) mDisposable.dispose();
    }

    @OnClick(R.id.button_sign_in)
    public void onSignInClicked() {
        if (NetworkUtil.isNetworkConnected(this)) {
            replaceButtonWithProgress(true);
            validateData();
        } else
            showToast(R.string.error_message_network_connection);
    }

    private void validateData() {
        String username = mUsernameEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();

        if (username.length() == 0 && password.length() == 0) {
            showToast(R.string.error_message_sign_in_blank);
            replaceButtonWithProgress(false);
        } else if (username.length() == 0) {
            showToast(R.string.error_message_sign_in_blank_username);
            replaceButtonWithProgress(false);
        } else if (password.length() == 0) {
            showToast(R.string.error_message_sign_in_blank_password);
            replaceButtonWithProgress(false);
        } else
            login(new LoginInfo(username, password));
    }

    private void showToast(int messageResource) {
        Toast.makeText(this, getString(messageResource), Toast.LENGTH_SHORT).show();
    }

    private void replaceButtonWithProgress(boolean isLoading) {
        mSignInProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        mSignInButton.setVisibility(isLoading ? View.GONE : View.VISIBLE);
        mUsernameEditText.setEnabled(!isLoading);
        mPasswordEditText.setEnabled(!isLoading);
    }

    private void login(LoginInfo loginInfo) {
        mDisposable = mDataManager.authenticate(loginInfo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(token -> {
                    if (!TextUtils.isEmpty(token))
                        startActivity(new Intent(ConnectActivity.this, ContentActivity.class));
                    else {
                        showToast(R.string.error_message_sign_in);
                        replaceButtonWithProgress(false);
                    }
                }, throwable -> {
                    showToast(R.string.error_message_sign_in);
                    replaceButtonWithProgress(false);
                });
    }

    @Override
    public boolean onSearchRequested() {
        // Start search activity
        return true;
    }

}
