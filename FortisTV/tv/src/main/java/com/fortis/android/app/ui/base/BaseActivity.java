package com.fortis.android.app.ui.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import com.fortis.android.app.injection.component.ActivityComponent;
import com.fortis.android.app.injection.component.DaggerActivityComponent;
import com.fortis.android.app.injection.component.DaggerFragmentComponent;
import com.fortis.android.app.injection.component.FragmentComponent;
import com.fortis.android.app.injection.module.ActivityModule;
import com.fortis.android.core.MainApplication;

@SuppressWarnings("deprecation")
public class BaseActivity extends Activity {

    private ActivityComponent mActivityComponent;
    private FragmentComponent mFragmentComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(MainApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    public FragmentComponent getFragmentComponent() {
        if (mFragmentComponent == null) {
            mFragmentComponent = DaggerFragmentComponent.builder()
                    .applicationComponent(MainApplication.get(this).getComponent())
                    .build();
        }
        return mFragmentComponent;
    }

}
