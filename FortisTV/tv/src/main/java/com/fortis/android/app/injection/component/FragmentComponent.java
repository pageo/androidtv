package com.fortis.android.app.injection.component;

import com.fortis.android.app.ui.content.ContentFragment;
import com.fortis.android.app.ui.search.SearchContentFragment;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.modules.FragmentModule;
import com.fortis.android.core.injection.modules.ProviderModule;
import com.fortis.android.core.injection.scopes.PerFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {FragmentModule.class, ProviderModule.class})
public interface FragmentComponent {
    void inject(ContentFragment contentFragment);
    void inject(SearchContentFragment searchContentFragment);
}
