package com.fortis.android.core.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.fortis.android.core.data.DataManager;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

public class LoginViewModelTest {
    // ----- Fields
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private DataManager mDataManager;

    @Captor
    private ArgumentCaptor<String> mTokenArgumentCaptor;

    @InjectMocks
    LoginViewModel mViewModel;

    // ----- Constructors
    public LoginViewModelTest() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    // ----- Tests
    @Test
    public void getToken_whenSignIn_OK() throws InterruptedException {

        // Arranges
        mViewModel.setEmail("opage");
        mViewModel.setPassword("xxxxx");
        when(mDataManager.authenticate(ArgumentMatchers.argThat(info -> info.loginOrEmail == "opage"))).thenReturn(Single.just("token"));

        // Acts
        Single<String> token = mViewModel.authenticate();

        // Assert
        token.test().assertValue("token");
    }

    @Test
    public void getNull_whenSignIn_KO() throws InterruptedException {

        // Arranges
        mViewModel.setEmail("opage");
        mViewModel.setPassword("xxxxx");
        when(mDataManager.authenticate(ArgumentMatchers.argThat(info -> info.loginOrEmail == "pat"))).thenReturn(Single.just("token"));

        // Acts
        Single<String> token = mViewModel.authenticate();

        // Asserts
        assertNull(token);
    }
}
