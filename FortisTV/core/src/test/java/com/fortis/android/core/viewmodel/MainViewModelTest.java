package com.fortis.android.core.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.User;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;

import static org.mockito.Mockito.when;

public class MainViewModelTest {

    // ----- Fields
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private DataManager mDataManager;

    @Captor
    private ArgumentCaptor<User> mUserArgumentCaptor;

    @InjectMocks
    MainViewModel mViewModel;

    // ----- Constructors
    public MainViewModelTest() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    // ----- Tests
    @Test
    public void getUserName_whenUserSaved() throws InterruptedException {

        // Arranges
        User user = new User();
        user.login = "opage";
        when(mDataManager.getUser()).thenReturn(Single.just(user));

        // Acts
        Single<User> value = mViewModel.getUser();

        // Assert
        value.test().assertValue(u -> u.login.equals("opage"));
    }
}
