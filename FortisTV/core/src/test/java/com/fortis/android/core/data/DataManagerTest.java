package com.fortis.android.core.data;

import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.data.remote.FortisService;
import com.fortis.android.core.test.common.TestDataFactory;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

public class DataManagerTest {

    @Mock
    FortisService mFortisService;

    @InjectMocks
    DataManager mDataManager;

    // ----- Constructors
    public DataManagerTest() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getFeedsCompletes() throws Exception {
        // Arranges
        List<Feed> mockFeeds = TestDataFactory.makeFeeds(10);
        stubDataManagerGetFeeds(Single.just(mockFeeds));

        // Acts
        TestObserver<List<Feed>> result = new TestObserver<>();
        mDataManager.getFeeds().subscribeWith(result);

        // Asserts
        result.assertNoErrors();
        result.assertValue(mockFeeds);
    }

    // ----- Internal logics
    private void stubDataManagerGetFeeds(Single<List<Feed>> single) {
        Map<String, String> map = new HashMap<>();
        when(mFortisService.getFeeds(map)).thenReturn(single);
    }
}
