package com.fortis.android.core.presenter.content;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.test.common.TestDataFactory;
import com.fortis.android.core.util.RxSchedulersOverrideRule;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Single;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ContentPresenterTest {

    @Rule
    public RxSchedulersOverrideRule rxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Mock
    ContentMvpView mContentMvpView;

    @Mock
    DataManager mDataManager;

    @InjectMocks
    ContentPresenter mContentPresenter;

    public ContentPresenterTest() {
        MockitoAnnotations.initMocks(this);

        mContentPresenter.attachView(mContentMvpView);
    }

    @After
    public void detachView() {
        mContentPresenter.detachView();
    }

    @Test
    public void getFeedsSuccessful() {
        // Arranges
        List<Feed> feeds = TestDataFactory.makeFeeds(10);
        stubDataManagerGetFeeds(Single.just(feeds));

        // Acts
        mContentPresenter.getFeeds();

        // Asserts
        verify(mContentMvpView).showFeeds(feeds);
        verify(mContentMvpView, never()).showFeedsError();
    }

    @Test
    public void getTagsFails() {
        // Arranges
        List<Feed> feeds = TestDataFactory.makeFeeds(10);
        stubDataManagerGetFeeds(Single.just(feeds));
        stubDataManagerGetFeeds(Single.error(new RuntimeException()));

        // Acts
        mContentPresenter.getFeeds();

        // Asserts
        verify(mContentMvpView).showFeedsError();
        verify(mContentMvpView, never()).showFeeds(anyListOf(Feed.class));
    }

    // ----- Internal logics
    private void stubDataManagerGetFeeds(Single<List<Feed>> single) {
        when(mDataManager.getFeeds()).thenReturn(single);
    }
}