package com.fortis.android.core.test.common.injection.module;

import com.fortis.android.core.data.remote.FortisService;
import com.fortis.android.core.injection.scopes.PerApplication;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module
public class NetTestModule {

    @Provides
    @PerApplication
    FortisService provideFortisService() {
        return mock(FortisService.class);
    }
}
