package com.fortis.android.core.test.common;

import com.fortis.android.core.data.model.Feed;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestDataFactory {

    public static String generateRandomString() {
        return UUID.randomUUID().toString().substring(0, 5);
    }

    public static Feed makeFeed(String unique) {
        Feed feed = new Feed();
        feed.name = "Name " + unique;
        feed.image = generateRandomString();
        return feed;
    }

    public static List<Feed> makeFeeds(int count) {
        List<Feed> feeds = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            feeds.add(makeFeed(String.valueOf(i)));
        }
        return feeds;
    }
}
