package com.fortis.android.core.test.common.injection.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.fortis.android.core.BuildConfig;
import com.fortis.android.core.injection.qualifiers.ApplicationContext;
import com.fortis.android.core.injection.scopes.PerApplication;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

@Module
public class ApplicationTestModule {

    private final Application mApplication;

    public ApplicationTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @PerApplication
    Resources provideResources() {
        return mApplication.getResources();
    }

    @Provides
    @PerApplication
    static RealmConfiguration provideRealmConfiguration() {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
        if (BuildConfig.DEBUG)
            builder = builder.deleteRealmIfMigrationNeeded();
        return builder.build();
    }

    @Provides
    static Realm provideRealm(RealmConfiguration realmConfiguration) {
        return Realm.getInstance(realmConfiguration);
    }
}