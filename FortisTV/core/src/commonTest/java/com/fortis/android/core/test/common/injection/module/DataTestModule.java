package com.fortis.android.core.test.common.injection.module;

import com.fortis.android.core.data.local.IFeedRepository;
import com.fortis.android.core.data.local.RealmFeedRepository;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataTestModule {
    @Binds
    abstract IFeedRepository bindFeedRepository(RealmFeedRepository realmFeedRepository);
}
