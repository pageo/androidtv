package com.fortis.android.core.test.common.injection.component;

import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.scopes.PerApplication;
import com.fortis.android.core.test.common.injection.module.ApplicationTestModule;
import com.fortis.android.core.test.common.injection.module.DataTestModule;
import com.fortis.android.core.test.common.injection.module.NetTestModule;

import dagger.Component;

@PerApplication
@Component(modules = { ApplicationTestModule.class, DataTestModule.class, NetTestModule.class})
public interface TestComponent extends ApplicationComponent {

}