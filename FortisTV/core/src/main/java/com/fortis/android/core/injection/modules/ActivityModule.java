package com.fortis.android.core.injection.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.fortis.android.core.injection.qualifiers.ActivityContext;
import com.fortis.android.core.injection.scopes.PerActivity;
import com.fortis.android.core.ui.base.AppCompatLifecycleActivity;
import com.fortis.android.core.ui.base.navigator.ActivityNavigator;
import com.fortis.android.core.ui.base.navigator.INavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private AppCompatLifecycleActivity mActivity;

    public ActivityModule(AppCompatLifecycleActivity activity) {
        mActivity = activity;
    }

    @Provides
    AppCompatLifecycleActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

    @Provides
    @PerActivity
    FragmentManager provideFragmentManager() {
        return mActivity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    INavigator provideNavigator() {
        return new ActivityNavigator(mActivity);
    }
}
