package com.fortis.android.core.injection.modules;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.fortis.android.core.injection.qualifiers.ActivityContext;
import com.fortis.android.core.injection.qualifiers.ChildFragmentManager;
import com.fortis.android.core.injection.qualifiers.DefaultFragmentManager;
import com.fortis.android.core.injection.scopes.PerFragment;
import com.fortis.android.core.ui.base.navigator.FragmentNavigator;
import com.fortis.android.core.ui.base.navigator.INavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    // ----- Fields
    private final Fragment mFragment;

    // ----- Constructors
    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    // ----- Methods
    @Provides
    @PerFragment
    @ActivityContext
    Context provideActivityContext() { return mFragment.getActivity(); }

    @Provides
    @PerFragment
    @DefaultFragmentManager
    FragmentManager provideDefaultFragmentManager() { return mFragment.getFragmentManager(); }

    @Provides
    @PerFragment
    @ChildFragmentManager
    FragmentManager provideChildFragmentManager() { return mFragment.getChildFragmentManager(); }

    @Provides
    @PerFragment
    INavigator provideNavigator() { return new FragmentNavigator(mFragment); }
}
