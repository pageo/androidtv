package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.local.IFeedRepository;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.injection.scopes.PerViewHolder;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

@PerViewHolder
public class FeedViewModel extends BaseViewModel {

    // ----- Fields
    protected IFeedRepository feedRepository;

    protected Feed feed;
    protected boolean isLast = false;

    // ----- Properties
    public Feed getFeed() {
        return feed;
    }

    public String getName() {
        return feed.name;
    }

    public String getImageUrl() {
        return feed.image;
    }

    public String getStatus() {
        return feed.status;
    }

    public String getAuthor() { return "B.P"; }

    // ----- Constructors
    @Inject
    public FeedViewModel(IFeedRepository feedRepository) {
        this.feedRepository = feedRepository;
    }

    // ----- Override methods
    public void update(Feed feed, boolean isLast) {
        this.isLast = isLast;
        this.feed = feed;
    }
}
