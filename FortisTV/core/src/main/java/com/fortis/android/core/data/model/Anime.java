package com.fortis.android.core.data.model;

public class Anime {
    public String imageUrl;
    public String name;
    public String pageId;
}
