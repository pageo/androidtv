package com.fortis.android.core.data.model;

import org.parceler.Parcel;

import io.realm.FeedRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Parcel(implementations = {FeedRealmProxy.class},
        value = Parcel.Serialization.FIELD,
        analyze = {Feed.class})
public class Feed extends RealmObject implements Comparable<Feed> {
    // ----- Fields
    @PrimaryKey
    public Integer id;
    public String name;
    public String image;
    public String status;
    public String profilePic;
    public String url;

    // ----- Override methods
    @Override
    public int compareTo(Feed another) {
        if (name != null && another.name != null) {
            return name.compareTo(another.name);
        } else {
            return 0;
        }
    }
}
