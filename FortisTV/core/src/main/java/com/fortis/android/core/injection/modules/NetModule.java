package com.fortis.android.core.injection.modules;

import com.fortis.android.core.AppConfig;
import com.fortis.android.core.BuildConfig;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.data.remote.FortisService;
import com.fortis.android.core.injection.scopes.PerApplication;
import com.fortis.android.core.util.FeedTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.tumblr.remember.Remember;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    // ----- Methods
    @Provides
    @PerApplication
    static Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Feed.class, FeedTypeAdapter.INSTANCE)
                .setLenient()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
                .create();
    }

    @Provides
    @PerApplication
    static OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.addInterceptor(loggingInterceptor);
        }
        OkHttpClient okHttpClient = okHttpClientBuilder
                .addInterceptor(new TokenInterceptor())
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .build();
        return okHttpClient;
    }

    @Provides
    @PerApplication
    static FortisService provideFortisService(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(AppConfig.FORTIS_ENDPOINT_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(FortisService.class);
    }

    // ----- Internal logic
    private static class TokenInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            String token = Remember.getString("Token", "");
            Request request = chain.request();
            request = request.newBuilder()
                    .addHeader("Token", token)
                    .build();
            return chain.proceed(request);
        }
    }
}
