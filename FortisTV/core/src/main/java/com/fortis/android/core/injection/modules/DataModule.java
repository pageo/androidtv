package com.fortis.android.core.injection.modules;

import com.fortis.android.core.data.local.IFeedRepository;
import com.fortis.android.core.data.local.RealmFeedRepository;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataModule {

    @Binds
    abstract IFeedRepository bindFeedRepository(RealmFeedRepository realmFeedRepository);
}
