package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.User;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class MainViewModel extends BaseViewModel {

    // ----- Fields
    private final DataManager dataManager;

    // ----- Constructors
    @Inject
    public MainViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    // ----- Public methods
    public Single<User> getUser() {
        return dataManager.getUser();
    }
}
