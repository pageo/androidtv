package com.fortis.android.core.providers.searchs;

import com.fortis.android.core.providers.Providers;
import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.data.model.MoviesPerPage;
import com.fortis.android.core.util.ProviderUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FMovieSearchProvider implements ISearchProvider {
    // ----- Fields
    private ProviderUtils utils;

    // ----- Constructors
    @Inject
    public FMovieSearchProvider(ProviderUtils providerUtils) {
        this.utils = providerUtils;
    }

    // ----- Public methods
    @Override
    public MoviesPerPage list() {
        String url = Providers.ONLINEMOVIES_LIST_URL;
        String responseBody = utils.getWebPage(url);
        //*[@id="center"]/div[3]/div/ul/li[1]/span
        String maxPages = Jsoup.parse(responseBody).select("body > div#container > div#center > div.pagination > div.pagination > ul > li > span").text().split(" ")[3];
        int max = Integer.parseInt(maxPages);
        MoviesPerPage moviesPerPage = new MoviesPerPage();
        for (int i = 1; i <= max; i++) {
            moviesPerPage.movies = listPerPage(i);
        }
        return moviesPerPage;
    }

    // ----- Public methods
    private List<Movie> listPerPage(Integer pageNumber) {
        String url = Providers.ONLINEMOVIES_LIST_URL + "page/" + pageNumber + "/?filtre=date";
        String responseBody = utils.getWebPage(url);
        Element searchResultsBox = isolate(responseBody);
        if (searchResultsBox == null) {
            //throw new Exception(new Throwable("Parsing failed."));
        }
        if (!hasSearchResults(searchResultsBox)) {
            //throw OnErrorThrowable.from(new Throwable("No search results."));
        }
        Elements searchResults = seperateResults(searchResultsBox);
        return parseResults(searchResults);
    }

    @Override
    public List<Movie> searchFor(String searchTerm) {
        return null;
    }

    @Override
    public Element isolate(String document) {
        return Jsoup.parse(document).select("body > div#wrapper > div#body-wrapper > div.container > div.widget > div.widget-body > div.row.movie-list").first();
    }

    @Override
    public boolean hasSearchResults(Element element) {
        return element.select("div").size() > 0;
    }

    // ----- Private methods
    private Elements seperateResults(Element searchResultsBox) {
        return searchResultsBox.children();
    }

    private List<Movie> parseResults(Elements searchResults) {
        List<Movie> movies = new ArrayList<>(searchResults.size());
        for (Element searchResult : searchResults) {
            Element item = searchResult.select("div.item").first();
            String url = Providers.ONLINEMOVIES_BASE_URL + item.select("a.poster").attr("href");
            String posterPath = item.select("a.poster > img").attr("src").trim();
            String title = item.select("a.name").text().trim();
            String quality = "";//item.select("div.quality").text().trim();

            Movie movie = new Movie();
            movie.setProviderType(Providers.ONLINEMOVIES);
            movie.setUrl(url);
            movie.setPosterPath(posterPath);
            movie.setTitle(title);
            movie.setQuality(quality);

            movies.add(movie);
        }
        return movies;
    }
}
