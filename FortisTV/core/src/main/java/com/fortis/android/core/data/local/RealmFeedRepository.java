package com.fortis.android.core.data.local;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;

import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.injection.scopes.PerApplication;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

@PerApplication
@SuppressLint("NewApi")
public class RealmFeedRepository implements IFeedRepository {
    // ----- Fields
    private final Provider<Realm> realmProvider;

    // ----- Constructors
    @Inject
    public RealmFeedRepository(Provider<Realm> realmProvider) {
        this.realmProvider = realmProvider;
    }

    // ----- Override methods
    @Override
    public Observable<String> getFavoriteChangeObservable() {
        return null;
    }

    @Override
    public List<Feed> findAllSorted(String sortField, Sort sort, boolean detached) {
        try (Realm realm = realmProvider.get()) {
            RealmResults<Feed> realmResults = realm.where(Feed.class).findAllSorted(sortField, sort);
            if (detached) {
                return realm.copyFromRealm(realmResults);
            } else {
                return realmResults;
            }
        }
    }

    @Override
    public Observable<List<Feed>> findAllSortedWithChanges(String sortField, Sort sort) {
        return null;
    }

    @Nullable
    @Override
    public Feed getByField(String field, String value, boolean detached) {
        try (Realm realm = realmProvider.get()) {
            Feed realmFeed = realm.where(Feed.class).equalTo(field, value).findFirst();
            if (detached && realmFeed != null) {
                realmFeed = realm.copyFromRealm(realmFeed);
            }
            return realmFeed;
        }
    }

    @Override
    public void save(Feed feed) {
        try (Realm realm = realmProvider.get()) {
            realm.executeTransaction(r -> r.copyToRealmOrUpdate(feed));
        }
    }

    @Override
    public void delete(Feed feed) {
        if (feed.isValid()) {
            try (Realm realm = realmProvider.get()) {
                realm.executeTransaction(r -> feed.deleteFromRealm());
            }
        }
    }

    @Override
    public Feed detach(Feed feed) {
        if (feed.isManaged()) {
            try (Realm realm = realmProvider.get()) {
                return realm.copyFromRealm(feed);
            }
        } else {
            return feed;
        }
    }
}
