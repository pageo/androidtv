package com.fortis.android.core.ui.base.navigator;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class ActivityNavigator extends BaseNavigator {

    // ----- Fields
    private final FragmentActivity activity;

    // ----- Constructors
    public ActivityNavigator(FragmentActivity activity) {
        this.activity = activity;
    }

    // ----- Override methods
    @Override
    protected final FragmentActivity getActivity() {
        return activity;
    }

    @Override
    final FragmentManager getChildFragmentManager() {
        throw new UnsupportedOperationException("Activities do not have a child fragment manager.");
    }
}
