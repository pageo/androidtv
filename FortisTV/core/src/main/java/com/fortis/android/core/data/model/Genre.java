package com.fortis.android.core.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Genre extends RealmObject implements Comparable<Genre> {
    // ----- Fields
    @PrimaryKey
    public Integer id;
    public String name;

    // ----- Overrides methods
    @Override
    public int compareTo(Genre other) {
        if (name != null && other.name != null) {
            return name.compareTo(other.name);
        } else {
            return 0;
        }
    }
}
