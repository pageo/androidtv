package com.fortis.android.core.viewmodel;

import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

public class SettingViewModel extends BaseViewModel {

    // ----- Fields
    private String mAboutTitle = "About";
    private String mSummary = "<br />Author:Olivier Page<br /><br />Github: <a href=\"https://github.com/opage\">opage</a>";
    private String mVersionTitle = "Version";

    // ----- Properties
    public String getAboutTitle() {
        return mAboutTitle;
    }

    public String getSummary() {
        return mSummary;
    }

    public String getVersionTitle() {
        return mVersionTitle;
    }
}
