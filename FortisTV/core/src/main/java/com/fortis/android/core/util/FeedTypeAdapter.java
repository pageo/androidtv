package com.fortis.android.core.util;

import com.fortis.android.core.data.model.Feed;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class FeedTypeAdapter extends TypeAdapter<Feed> {

    // ----- Fields
    public static final TypeAdapter<Feed> INSTANCE = new FeedTypeAdapter().nullSafe();

    // ----- Constructors
    private FeedTypeAdapter() {
    }

    // ----- Override methods
    @Override
    public void write(JsonWriter out, Feed value) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Feed read(JsonReader in) throws IOException {
        try {
            Feed feed = new Feed();
            in.beginObject();
            while (in.hasNext()) {
                String name = in.nextName();
                switch (name) {
                    case "id": {
                        // cannot be null, because it is the primary key
                        feed.id = in.nextInt();
                        break;
                    }
                    case "name": {
                        feed.name = JsonUtils.readNullSafeString(in);
                        break;
                    }
                    case "status": {
                        feed.status = JsonUtils.readNullSafeString(in);
                        break;
                    }
                    case "url": {
                        feed.url = JsonUtils.readNullSafeString(in);
                        break;
                    }
                    case "profilePic": {
                        feed.profilePic = JsonUtils.readNullSafeString(in);
                        break;
                    }
                    case "image": {
                        feed.image = JsonUtils.readNullSafeString(in);
                        break;
                    }
                    default:
                        in.skipValue();
                        break;
                }
            }
            in.endObject();
            return feed;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }
}
