package com.fortis.android.core.ui.base.navigator;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

public interface INavigator {
    String EXTRA_ARGS = "_args";

    void finishActivity();

    void startActivity(@NonNull Intent intent);

    void startActivity(@NonNull String action);

    void startActivity(@NonNull String action, @NonNull Uri uri);

    void startActivity(@NonNull Class<? extends Activity> activityClass);

    void startActivity(@NonNull Class<? extends Activity> activityClass, Bundle args);

    void startActivity(@NonNull Class<? extends Activity> activityClass, Parcelable args);

    void startFragmentActivity(@NonNull Class<? extends FragmentActivity> fragmentActivityClass);

    void startFragmentActivity(@NonNull Class<? extends FragmentActivity> fragmentActivityClass, Bundle args);

    void startFragmentActivity(@NonNull Class<? extends FragmentActivity> fragmentActivityClass, Parcelable args);

    void replaceFragment(@IdRes int containerId, @NonNull Fragment fragment, Bundle args);

    void replaceFragment(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args);

    void replaceFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, Bundle args, String backstackTag);

    void replaceFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args, String backstackTag);

    void replaceChildFragment(@IdRes int containerId, @NonNull Fragment fragment, Bundle args);

    void replaceChildFragment(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args);

    void replaceChildFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, Bundle args, String backstackTag);

    void replaceChildFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args, String backstackTag);

}
