package com.fortis.android.core.presenter.content;

import com.fortis.android.core.data.model.Anime;
import com.fortis.android.core.ui.base.presenter.MvpView;
import com.fortis.android.core.data.model.Feed;

import java.util.List;

public interface ContentMvpView extends MvpView {

    void showFeeds(List<Feed> feeds);

    void showFeedsError();

    void showAnimes(List<Anime> animes);

    void showAnimesError();

}