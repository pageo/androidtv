package com.fortis.android.core.util;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import com.fortis.android.core.bus.BusProvider;
import com.fortis.android.core.bus.event.SnackbarEvent;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ProviderUtils {
    // ----- Fields
    OkHttpClient okHttpClient;

    // ----- Constructors
    @Inject
    public ProviderUtils(@Named("okHttpClientForSource") OkHttpClient okHttpClient){
        this.okHttpClient = okHttpClient;
    }

    // ----- Public methods
    public Response makeRequest(final Request request) throws IOException {
        return okHttpClient.newCall(request).execute();
    }

    public String getWebPage(final String url) {
        return getWebPage(new Request.Builder().url(url).get().build());
    }

    public String getWebPage(final Request request) {
        try {
            return makeRequest(request).body().string();
        } catch (IOException e) {
            return null;
        }
    }

    public static String encodeForUtf8(String s) {
        try {
            return URLEncoder.encode(s, "utf-8");
        } catch (UnsupportedEncodingException u) {
            u.printStackTrace();
            return s.replace(":", "%3A")
                    .replace("/", "%2F")
                    .replace("#", "%23")
                    .replace("?", "%3F")
                    .replace("&", "%24")
                    .replace("@", "%40")
                    .replace("%", "%25")
                    .replace("+", "%2B")
                    .replace(" ", "+")
                    .replace(";", "%3B")
                    .replace("=", "%3D")
                    .replace("$", "%26")
                    .replace(",", "%2C")
                    .replace("<", "%3C")
                    .replace(">", "%3E")
                    .replace("~", "%25")
                    .replace("^", "%5E")
                    .replace("`", "%60")
                    .replace("\\", "%5C")
                    .replace("[", "%5B")
                    .replace("]", "%5D")
                    .replace("{", "%7B")
                    .replace("|", "%7C")
                    .replace("\"", "%22");
        }
    }

    public static void lazyDownload(AppCompatActivity activity, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        } else {
            BusProvider.post(new SnackbarEvent("No app to open this link."));
        }
    }

    public static String formatError(Throwable e) {
        if (e.getMessage() != null) {
            return "Error: " + e.getMessage().replace("java.lang.Throwable:", "").trim();
        }
        return "An error occurred.";
    }

    public static String jwPlayerIsolate(String body) {
        String javascriptShit = Jsoup.parse(body).select("div#player_code").first().child(0).html();
        String almostVideoURL = javascriptShit.substring(javascriptShit.indexOf("\"file\": \"") + 9);
        return almostVideoURL.substring(0, almostVideoURL.indexOf("\","));
    }

    public static String formattedGenres(String[] genres) {
        StringBuilder genresBuilder = new StringBuilder();
        for (String genre : genres) {
            genresBuilder.append(" ");
            genresBuilder.append(genre);
            genresBuilder.append(",");
        }
        genresBuilder.deleteCharAt(genresBuilder.length() - 1);
        return genresBuilder.toString();
    }
}
