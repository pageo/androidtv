package com.fortis.android.core.injection.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.fortis.android.core.BuildConfig;
import com.fortis.android.core.injection.qualifiers.ApplicationContext;
import com.fortis.android.core.injection.scopes.PerApplication;
import com.fortis.android.core.util.ToastPresenter;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @PerApplication
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() { return mApplication; }

    @Provides
    @PerApplication
    Resources provideResources() {
        return mApplication.getResources();
    }

    @Provides
    @PerApplication
    ToastPresenter provideToastPresenter() {
        return new ToastPresenter(mApplication);
    }

    @Provides
    @PerApplication
    static RealmConfiguration provideRealmConfiguration() {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
        if (BuildConfig.DEBUG)
            builder = builder.deleteRealmIfMigrationNeeded();
        return builder.build();
    }

    @Provides
    static Realm provideRealm(RealmConfiguration realmConfiguration) {
        return Realm.getInstance(realmConfiguration);
    }
}
