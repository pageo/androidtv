package com.fortis.android.core.util;

import android.content.Context;
import android.widget.Toast;

import com.fortis.android.core.injection.qualifiers.ApplicationContext;

import javax.inject.Inject;

public class ToastPresenter {
    // ----- Fields
    private final Context mContext;

    // ----- Constructors
    @Inject
    public ToastPresenter(@ApplicationContext Context appContext) {
        mContext = appContext;
    }

    // ----- Public methods
    public void showShortToast(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
