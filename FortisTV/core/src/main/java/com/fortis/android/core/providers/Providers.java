package com.fortis.android.core.providers;


import com.fortis.android.core.providers.sources.FMovieSourceProvider;
import com.fortis.android.core.providers.sources.ISourceProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Providers {

    // Codes
    public static final int ONLINEMOVIES = 0;

    // Titles
    public static final String ONLINEMOVIES_TITLE = "ONLINEMOVIES";

    // Base URLs
    public static final String ONLINEMOVIES_BASE_URL = "http://onlinemovies.is";

    // Base search paths (must start with / )
    public static final String ONLINEMOVIES_SEARCH_EXT = "/search/";

    // Base list paths
    public static final String ONLINEMOVIES_LIST_EXT = "/category/cinema-movies";

    // Base search URLs
    public static final String ONLINEMOVIES_SEARCH_URL = ONLINEMOVIES_BASE_URL + ONLINEMOVIES_SEARCH_EXT;

    // Base list URLs
    public static final String ONLINEMOVIES_LIST_URL = ONLINEMOVIES_BASE_URL + ONLINEMOVIES_LIST_EXT;

    public static final Map<String, ISourceProvider> SOURCE_MAP = getSourceList();

    private static Map<String, ISourceProvider> getSourceList () {
        Map<String, ISourceProvider> sourceMap = new HashMap<>();
        sourceMap.put(ONLINEMOVIES_TITLE, new FMovieSourceProvider());
        return sourceMap;
    }

    public static final ArrayList<String> ALL_PROVIDER_TITLES = getAllProviderTitles();

    private static ArrayList<String> getAllProviderTitles() {
        ArrayList<String> titles = new ArrayList<>(1);
        titles.add(ONLINEMOVIES_TITLE.toString());
        return titles;
    }
}
