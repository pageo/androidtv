package com.fortis.android.core.data.model;

public class RegisterInfo {
    // ----- Fields
    public String fullName;
    public String loginOrEmail;
    public String password;

    // ----- Constructors
    public RegisterInfo(String fullName, String loginOrEmail, String password) {
        this.fullName = fullName;
        this.loginOrEmail = loginOrEmail;
        this.password = password;
    }
}
