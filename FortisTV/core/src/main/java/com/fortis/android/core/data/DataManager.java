package com.fortis.android.core.data;

import android.text.TextUtils;

import com.fortis.android.core.data.model.Anime;
import com.fortis.android.core.data.model.Comment;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.data.model.LoginInfo;
import com.fortis.android.core.data.model.RegisterInfo;
import com.fortis.android.core.data.model.User;
import com.fortis.android.core.data.remote.FortisService;
import com.fortis.android.core.injection.scopes.PerApplication;
import com.fortis.android.core.util.PreferencesHelper;
import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;

@PerApplication
public class DataManager {

    private final FortisService mFortisService;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManager(PreferencesHelper preferencesHelper, FortisService fortisService) {
        mPreferencesHelper = preferencesHelper;
        mFortisService = fortisService;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public Single<List<Anime>> getAnimes() {
        return mFortisService.getAnimes();
    }

    public Single<List<Feed>> getFeeds() {
        Map<String, String> map = new HashMap<>();
        return mFortisService.getFeeds(map);
    }

    public Single<String> authenticate(LoginInfo loginInfo) {
        return mFortisService
                .authenticate(loginInfo)
                .map(token -> {
                    Remember.remove("Token");
                    if (!TextUtils.isEmpty(token))
                        Remember.putString("Token", !token.endsWith("==") ? token.concat("==") : token);
                    return token;
                });
    }

    public Single<User> getUser() {
        return mFortisService.getUser();
    }

    public Single<String> register(RegisterInfo registerInfo) {
        return mFortisService.register(registerInfo);
    }

    public Single<List<Comment>> getComments(int id, int perPage, int page) {
        User user = new User();
        user.login = "opage";
        user.fullName = "Olivier Page";
        user.avatar = "https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-simple-512.png";

        List<Comment> comments = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Comment comment = new Comment();
            comment.id = i;
            comment.user = user;
            comment.body = "Thanks bro";
            comments.add(comment);
        }
        return Single.just(comments);
    }
}
