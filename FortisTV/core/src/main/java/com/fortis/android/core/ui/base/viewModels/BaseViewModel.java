package com.fortis.android.core.ui.base.viewModels;

import android.databinding.BaseObservable;

public abstract class BaseViewModel extends BaseObservable {

    public BaseViewModel() {
        super();
    }
}
