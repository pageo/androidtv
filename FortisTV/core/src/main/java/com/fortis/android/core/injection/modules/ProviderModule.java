package com.fortis.android.core.injection.modules;

import android.content.Context;

import com.fortis.android.core.util.PersistentCookieStore;
import com.fortis.android.core.util.ProviderUtils;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class ProviderModule {
    // ----- Fields
    private static Context mContext;

    // ----- Constructors
    public ProviderModule(Context context) {
        mContext = context;
    }

    @Provides
    @Named("okHttpClientForSource")
    static OkHttpClient provideOkHttpClientForSource() {
        CookieHandler cookieHandler = new CookieManager(new PersistentCookieStore(mContext), CookiePolicy.ACCEPT_ALL);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .addInterceptor(logging)
                .build();
        return okHttpClient;
    }

    @Provides
    static ProviderUtils provideProviderUtils(@Named("okHttpClientForSource") OkHttpClient okHttpClient) {
        return new ProviderUtils(okHttpClient);
    }
}
