package com.fortis.android.core.injection.components;

import android.content.Context;
import android.content.res.Resources;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.local.IFeedRepository;
import com.fortis.android.core.injection.modules.ApplicationModule;
import com.fortis.android.core.injection.modules.DataModule;
import com.fortis.android.core.injection.modules.NetModule;
import com.fortis.android.core.injection.modules.ProviderModule;
import com.fortis.android.core.injection.qualifiers.ApplicationContext;
import com.fortis.android.core.injection.scopes.PerApplication;
import com.fortis.android.core.util.PreferencesHelper;
import com.fortis.android.core.util.ToastPresenter;

import dagger.Component;
import io.realm.Realm;

@PerApplication
@Component(modules = { ApplicationModule.class, NetModule.class, DataModule.class, ProviderModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Resources resources();

    ToastPresenter toastPresenter();

    Realm realm();

    IFeedRepository feedRepository();

    PreferencesHelper preferencesHelper();

    DataManager dataManager();
}
