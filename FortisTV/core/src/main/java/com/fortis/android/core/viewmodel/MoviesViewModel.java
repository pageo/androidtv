package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.model.MoviesPerPage;
import com.fortis.android.core.injection.scopes.PerFragment;
import com.fortis.android.core.providers.searchs.FMovieSearchProvider;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.realm.internal.IOException;

@PerFragment
public class MoviesViewModel extends BaseViewModel {

    // ----- Fields
    private FMovieSearchProvider searchProvider;

    // ----- Constructors
    @Inject
    public MoviesViewModel(FMovieSearchProvider searchProvider) {
        this.searchProvider = searchProvider;
    }

    // ----- Public  methods
    public Observable<MoviesPerPage> getMoviesPerPage() {
        return Observable.defer(() -> {
            try {
                return Observable.just(searchProvider.list());
            } catch (IOException io) {
                io.printStackTrace();
                return Observable.error(io);
            }
        });
    }
}
