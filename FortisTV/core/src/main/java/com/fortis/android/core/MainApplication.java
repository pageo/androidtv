package com.fortis.android.core;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.components.DaggerApplicationComponent;
import com.fortis.android.core.injection.modules.ApplicationModule;
import com.tumblr.remember.Remember;

import io.realm.Realm;
import timber.log.Timber;

public class MainApplication extends Application {

    static MainApplication sInstance = null;
    static ApplicationComponent mApplicationComponent = null;

    @Override
    public void onCreate() {
        super.onCreate();

        // Realm
        Realm.init(this);

        //SharedPreferences Storage
        Remember.init(this, "com.fortis.android");

        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        sInstance = this;
    }

    public static MainApplication get(Context context) {
        return (MainApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    public static Realm getRealm() {
        return mApplicationComponent.realm();
    }

    public static Resources getRes() {
        return sInstance.getResources();
    }
}
