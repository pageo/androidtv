package com.fortis.android.core.presenter.content;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.ui.base.presenter.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ContentPresenter extends BasePresenter<ContentMvpView> {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private final DataManager mDataManager;

    @Inject
    public ContentPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mCompositeDisposable != null) mCompositeDisposable.dispose();
    }

    public void getFeeds() {
        checkViewAttached();
        mCompositeDisposable.add(mDataManager.getFeeds()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(feeds -> getMvpView().showFeeds(feeds),
                        throwable -> {
                            getMvpView().showFeedsError();
                            Timber.e(throwable, "There was an error loading the feeds!");
                        }));
    }

    public void getAnimes() {
        checkViewAttached();
        mCompositeDisposable.add(mDataManager.getAnimes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(animes -> getMvpView().showAnimes(animes),
                        throwable -> {
                            getMvpView().showAnimesError();
                            Timber.e(throwable, "There was an error loading the animes!");
                        }));    }

}