package com.fortis.android.core.viewmodel;

import android.support.annotation.Nullable;

import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.injection.scopes.PerViewHolder;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import java.util.Date;

import javax.inject.Inject;

@PerViewHolder
public class MovieViewModel extends BaseViewModel {
    // ----- Fields
    protected Movie movie;

    // ----- Properties
    public Movie getMovie() {
        return movie;
    }

    public String getTitle() {
        return movie.getTitle();
    }

    @Nullable
    public Date getReleaseDate() {
        return movie.getReleaseDate();
    }

    public String getPosterPath() {
        return movie.getPosterPath();
    }

    // ----- Constructors
    @Inject
    public MovieViewModel() {}

    // ----- Override methods
    public void update(Movie movie) {
        this.movie = movie;
    }
}
