package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.RegisterInfo;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class RegisterViewModel extends BaseViewModel {

    // ----- Fields
    private String mFullName = "";
    private String mEmail = "";
    private String mPassword = "";
    private final DataManager dataManager;

    // ----- Properties
    public String getFullName() {
        return this.mFullName;
    }

    public void setFullName(String fullName) {
        this.mFullName = fullName;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getPassword() {
        return this.mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    // ----- Constructors
    @Inject
    public RegisterViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    // ----- Public methods
    public Single<String> register() {
        return dataManager.register(new RegisterInfo(getFullName(), getEmail(), getPassword()));
    }
}
