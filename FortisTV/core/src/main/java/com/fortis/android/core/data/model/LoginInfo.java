package com.fortis.android.core.data.model;

public class LoginInfo {
    // ----- Fields
    public String loginOrEmail;
    public String password;

    // ----- Constructors
    public LoginInfo(String loginOrEmail, String password) {
        this.loginOrEmail = loginOrEmail;
        this.password = password;
    }
}
