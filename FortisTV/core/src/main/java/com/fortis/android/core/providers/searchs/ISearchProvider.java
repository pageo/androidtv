package com.fortis.android.core.providers.searchs;

import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.data.model.MoviesPerPage;

import org.jsoup.nodes.Element;

import java.util.List;

public interface ISearchProvider {
    MoviesPerPage list();

    List<Movie> searchFor(String searchTerm);

    Element isolate(String document);

    boolean hasSearchResults(Element element);
}
