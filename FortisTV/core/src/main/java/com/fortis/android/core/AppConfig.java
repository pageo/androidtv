package com.fortis.android.core;

public class AppConfig {
    // Base API url
    public static String FORTIS_ENDPOINT_URL = "http://192.168.0.29:80/Fortis.CommonHost.Core/";
    //public static String FORTIS_ENDPOINT_URL = "http://192.168.43.44:80/Fortis.CommonHost.Core/";
    //public static String FORTIS_ENDPOINT_URL = "http://10.62.20.45:80/Fortis.CommonHost.Core/";

    // Moviedb API url
    public static String MOVIE_DB_ENDPOINT_URL = "https://api.themoviedb.org/";
    public static String MOVIE_DB_API_KEY = "817a5083104146de509671027f3b0504";

    public static final String baseImageUrl = "https://image.tmdb.org/t/p/w1280";
}
