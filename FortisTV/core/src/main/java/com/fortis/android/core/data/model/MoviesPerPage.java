package com.fortis.android.core.data.model;

import java.util.List;

public class MoviesPerPage {
    // ----- Fields
    public Integer page;
    public List<Movie> movies;
}
