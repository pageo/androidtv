package com.fortis.android.core.viewmodel;

import android.databinding.ObservableBoolean;

import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.injection.scopes.PerActivity;

import javax.inject.Inject;

import io.reactivex.Observable;

@PerActivity
public class MovieDetailViewModel extends MovieViewModel {

    // ----- Fields
    private final ObservableBoolean loaded = new ObservableBoolean();

    @Inject
    public MovieDetailViewModel() {}

    // ----- Override methods
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    public Observable<Movie> getTheMovie() {
        //return Observable.defer(() -> Observable.just(fMovieProvider.fetchMovie(getMovie().getUrl())));
        throw new UnsupportedOperationException();
    }
}
