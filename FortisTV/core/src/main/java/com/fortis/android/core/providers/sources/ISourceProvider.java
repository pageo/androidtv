package com.fortis.android.core.providers.sources;

import com.fortis.android.core.data.model.Video;

import java.util.List;

public interface ISourceProvider {
    List<Video> fetchSource(String embedPageUrl);
}
