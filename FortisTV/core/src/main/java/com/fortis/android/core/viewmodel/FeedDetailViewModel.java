package com.fortis.android.core.viewmodel;

import android.databinding.ObservableBoolean;

import com.fortis.android.core.data.local.IFeedRepository;
import com.fortis.android.core.injection.scopes.PerActivity;

import javax.inject.Inject;

@PerActivity
public class FeedDetailViewModel extends FeedViewModel {

    // ----- Fields
    private final ObservableBoolean loaded = new ObservableBoolean();

    // ----- Constructors
    @Inject
    public FeedDetailViewModel(IFeedRepository feedRepository) {
        super(feedRepository);
    }

    public ObservableBoolean isLoaded() {
        return loaded;
    }
}
