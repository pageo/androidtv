package com.fortis.android.core.ui.base.navigator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class FragmentNavigator extends BaseNavigator {

    // ----- Fields
    private final Fragment fragment;

    // ----- Constructors
    public FragmentNavigator(Fragment fragment) {
        this.fragment = fragment;
    }

    // ----- Override methods
    @Override
    protected final FragmentActivity getActivity() {
        return fragment.getActivity();
    }

    @Override
    final FragmentManager getChildFragmentManager() {
        return fragment.getChildFragmentManager();
    }
}
