package com.fortis.android.core.presenter.search;

import com.fortis.android.core.ui.base.presenter.MvpView;
import com.fortis.android.core.data.model.Feed;

import java.util.List;

public interface SearchContentMvpView extends MvpView {

    void showFeeds(List<Feed> feeds);

    void showFeedsError();

}