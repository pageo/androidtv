package com.fortis.android.core.data.model;

import org.parceler.Parcel;

@Parcel(value = Parcel.Serialization.FIELD,
        analyze = {Comment.class})
public class Comment {
    public int id;
    public String body;
    public User user;
}
