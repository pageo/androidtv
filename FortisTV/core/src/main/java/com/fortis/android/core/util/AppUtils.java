package com.fortis.android.core.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;

public class AppUtils {

    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    public static <T> T checkNotNull(T object, String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
        return object;
    }

    public static String getVersion(Context context) {
        String version;
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "UnknownVersion";
        }
        return version;
    }

    public static int dp2px(Context context, int dpValue) {
        return (int) context.getResources().getDisplayMetrics().density * dpValue;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Spanned getSpannedText(String text) {
        Spanned descriptionHtml;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            descriptionHtml = Html.fromHtml(text == null ? "" : text, Html.FROM_HTML_MODE_LEGACY);
        else
            descriptionHtml = Html.fromHtml(text == null ? "" : text);
        return descriptionHtml;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static final int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            return ContextCompat.getColor(context, id);
        else
            return context.getResources().getColor(id);
    }
}
