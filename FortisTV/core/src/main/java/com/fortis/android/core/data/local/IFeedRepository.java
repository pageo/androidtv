package com.fortis.android.core.data.local;

import android.support.annotation.Nullable;

import com.fortis.android.core.data.model.Feed;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Sort;

public interface IFeedRepository {
    Observable<String> getFavoriteChangeObservable();

    List<Feed> findAllSorted(String sortField, Sort sort, boolean detached);

    Observable<List<Feed>> findAllSortedWithChanges(String sortField, Sort sort);

    @Nullable
    Feed getByField(String field, String value, boolean detached);

    void save(Feed feed);

    void delete(Feed feed);

    Feed detach(Feed feed);
}
