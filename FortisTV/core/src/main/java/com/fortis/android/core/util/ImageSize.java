package com.fortis.android.core.util;

public class ImageSize {
    // ----- Fields
    public static final int[] NORMAL = new int[]{400, 300};
    public static final int[] LARGE = new int[]{800, 600};
    public static final int[] FULL = new int[]{480, 800};
}
