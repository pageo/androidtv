package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

//@PerFragment
public class FeedsViewModel extends BaseViewModel {

    // ----- Fields
    private final DataManager dataManager;

    // ----- Constructors
    @Inject
    public FeedsViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    // ----- Public  methods
    public Single<List<Feed>> getFeeds() {
        return dataManager.getFeeds();
    }
}
