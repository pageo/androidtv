package com.fortis.android.core.ui.base.presenter;

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}