package com.fortis.android.core.ui.base.viewModels;

import javax.inject.Inject;

public final class NoOpViewModel extends BaseViewModel {

    // ----- Constructors
    @Inject
    public NoOpViewModel() { }
}
