package com.fortis.android.core.presenter.search;

import com.fortis.android.core.ui.base.presenter.BasePresenter;
import com.fortis.android.core.data.DataManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SearchContentPresenter extends BasePresenter<SearchContentMvpView> {

    private Disposable mDisposable;
    private final DataManager mDataManager;

    @Inject
    public SearchContentPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mDisposable != null) mDisposable.dispose();
    }

    public void searchFeeds() {
        checkViewAttached();

        mDisposable = mDataManager.getFeeds()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(feeds -> getMvpView().showFeeds(feeds),
                        throwable -> {
                            getMvpView().showFeedsError();
                            Timber.e(throwable, "There was an error loading the cats!");
                        });
    }

}