package com.fortis.android.core.data.model;

import com.fortis.android.core.util.RealmListParcelConverter;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;

import io.realm.MovieRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Parcel(implementations = {MovieRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {Movie.class})
public class Movie extends RealmObject implements Comparable<Movie> {
    // ----- Fields
    @PrimaryKey
    private Integer id;
    private String title;
    private Date releaseDate;
    private String url;
    private String quality;
    private String posterPath;
    private Integer providerType;
    private RealmList<Video> videos;

    // ----- Properties
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Integer getProviderType() {
        return providerType;
    }

    public void setProviderType(Integer providerType) {
        this.providerType = providerType;
    }

    public RealmList<Video> getVideos() {
        return videos;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public void setVideos(RealmList<Video> videos) {
        this.videos = videos;
    }

    // ----- Constructors
    public void Movie(){}

    // ----- Override methods
    @Override
    public int compareTo(Movie movie) {
        if (title != null && movie.title != null) {
            return title.compareTo(movie.title);
        } else {
            return 0;
        }
    }
}
