package com.fortis.android.core.viewmodel;

import com.fortis.android.core.data.DataManager;
import com.fortis.android.core.data.model.LoginInfo;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class LoginViewModel extends BaseViewModel {

    // ----- Fields
    private String mEmail;
    private String mPassword;
    private final DataManager dataManager;

    // ----- Properties
    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    // ----- Constructors
    @Inject
    public LoginViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    // ----- Public methods
    public Single<String> authenticate() {
        return dataManager.authenticate(new LoginInfo(getEmail(), getPassword()));
    }
}
