package com.fortis.android.core.data.model;

import org.parceler.Parcel;

import java.util.Date;

@Parcel(value = Parcel.Serialization.FIELD,
        analyze = {User.class})
public class User {
    public String login;
    public String email;
    public String fullName;
    public String avatar;
    public Date lastLoginDate;
    public Boolean isAdmin;
}
