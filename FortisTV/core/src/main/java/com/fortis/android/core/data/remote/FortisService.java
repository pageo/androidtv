package com.fortis.android.core.data.remote;

import com.fortis.android.core.data.model.Anime;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.data.model.LoginInfo;
import com.fortis.android.core.data.model.RegisterInfo;
import com.fortis.android.core.data.model.User;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface FortisService {

    static String FORTIS_ENDPOINT_URL = "http://192.168.0.29:80/Fortis.CommonHost.Core/";

    @POST("api/v1/authenticate")
    Single<String> authenticate(@Body LoginInfo loginInfo);

    @POST("api/v1/register")
    Single<String> register(@Body RegisterInfo registerInfo);

    @GET("api/v1/user")
    Single<User> getUser();

    @GET("api/v1/feeds")
    Single<List<Feed>> getFeeds(@QueryMap Map<String, String> map);

    @GET("api/v1/animes")
    Single<List<Anime>> getAnimes();
}
