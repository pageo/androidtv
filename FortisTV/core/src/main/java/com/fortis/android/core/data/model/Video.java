package com.fortis.android.core.data.model;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.VideoRealmProxy;

@Parcel(implementations = {VideoRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {Video.class})
public class Video extends RealmObject {
    // ----- Fields
    private String title;
    private String url;

    // ----- Properties
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    // ----- Constructors
    public Video(){}
    public Video(String title, String url) {
        this.title = title;
        this.url = url;
    }
}
