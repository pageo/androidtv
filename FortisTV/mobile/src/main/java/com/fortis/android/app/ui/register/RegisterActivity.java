package com.fortis.android.app.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityRegisterBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.login.LoginActivity;
import com.fortis.android.app.ui.main.MainActivity;
import com.fortis.android.core.ui.base.navigator.INavigator;
import com.fortis.android.core.util.ToastPresenter;
import com.fortis.android.core.viewmodel.RegisterViewModel;
import com.tumblr.remember.Remember;

import javax.inject.Inject;

import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class RegisterActivity extends BaseActivity<ActivityRegisterBinding, RegisterViewModel> {

    // ----- Fields
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    INavigator navigator;

    @Inject
    ToastPresenter toastPresenter;

    // ----- Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_register, savedInstanceState);
    }

    // ----- Callback methods
    @OnClick(R.id.btnLinkToLoginScreen)
    void onLinkToLoginClickListener() {
        navigator.startActivity(new Intent(this, LoginActivity.class));
        navigator.finishActivity();
    }

    @OnClick(R.id.btnRegister)
    void onRegisterButtonClick() {
        if (!viewModel.getFullName().isEmpty() && !viewModel.getEmail().isEmpty() && !viewModel.getPassword().isEmpty()) {
            registerUser();
        } else {
            toastPresenter.showLongToast("Please enter your details!");
        }
    }

    // ----- Internal logic
    private void registerUser() {
        compositeDisposable.clear();
        compositeDisposable.add(viewModel.register()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(token -> {
                            if (!TextUtils.isEmpty(token)) {
                                registerSuccess();
                                Remember.putString("Token", !token.endsWith("==") ? token.concat("==") : token);
                            }
                            return;
                        },
                        throwable -> registerFailure()));
    }

    private void registerSuccess() {
        navigator.startActivity(new Intent(this, MainActivity.class));
        navigator.finishActivity();
    }

    private void registerFailure() {
        toastPresenter.showShortToast("register in failed , please try again .");
    }
}
