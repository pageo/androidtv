package com.fortis.android.app.ui.base;

import android.arch.lifecycle.LifecycleFragment;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortis.android.app.BR;
import com.fortis.android.app.injection.components.DaggerFragmentComponent;
import com.fortis.android.app.injection.components.FragmentComponent;
import com.fortis.android.core.MainApplication;
import com.fortis.android.core.injection.modules.FragmentModule;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import butterknife.ButterKnife;

@SuppressWarnings("rawtypes")
public abstract class BaseFragment<B extends ViewDataBinding, V extends BaseViewModel> extends LifecycleFragment {

    // ----- Fields
    protected B binding;

    @Inject
    protected V viewModel;

    private FragmentComponent mFragmentComponent;

    // ----- Methods
    @SuppressWarnings("deprecation")
    protected final FragmentComponent fragmentComponent() {
        if(mFragmentComponent == null) {
            mFragmentComponent = DaggerFragmentComponent.builder()
                    .fragmentModule(new FragmentModule(this))
                    .applicationComponent(MainApplication.get(getContext()).getComponent())
                    .build();
        }
        return mFragmentComponent;
    }

    protected final View setAndBindContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @LayoutRes int layoutResId, Bundle savedInstanceState) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must not be null and should be injected via fragmentComponent().inject(this)"); }
        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false);
        binding.setVariable(BR.vm, viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        viewModel = null;
    }

    @Override
    @CallSuper
    public void onDestroy() {
        mFragmentComponent = null;
        super.onDestroy();
    }
}
