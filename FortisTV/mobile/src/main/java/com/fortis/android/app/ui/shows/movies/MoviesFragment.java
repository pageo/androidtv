package com.fortis.android.app.ui.shows.movies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.FragmentRecyclerviewBinding;
import com.fortis.android.app.ui.base.BaseFragment;
import com.fortis.android.core.viewmodel.MoviesViewModel;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MoviesFragment extends BaseFragment<FragmentRecyclerviewBinding, MoviesViewModel>  {

    // ----- Fields
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    MovieAdapter adapter;

    // ----- Override methods
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, R.layout.fragment_recyclerview, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 5));
        binding.recyclerView.setAdapter(adapter);
        binding.swipeRefreshLayout.setOnRefreshListener(() -> reloadData());
        if (savedInstanceState == null)
            binding.swipeRefreshLayout.post(() -> binding.swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
    }

    @Override
    public void onPause() {
        unBind();
        super.onPause();
    }

    // ----- Internal logics
    private void bind() {
        mCompositeDisposable.add(viewModel.getMoviesPerPage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moviesPerPage -> {
                    adapter.setMovieList(moviesPerPage.movies);
                    adapter.notifyDataSetChanged();
                    onRefresh(false);
                }, throwable -> {
                    Timber.e(throwable, "Could not load movies");
                    onRefresh(false, true);
                }));
    }

    private void unBind() {
        mCompositeDisposable.clear();
    }

    private void reloadData(){
        unBind();
        bind();
    }

    public void onRefresh(boolean... params) {
        binding.swipeRefreshLayout.setRefreshing(params[0]);
        if (params.length >= 2 && params[1]) {
            Snackbar.make(binding.recyclerView, "Could not load movies", Snackbar.LENGTH_SHORT)
                    .setAction(R.string.snackbar_action_retry, v -> reloadData())
                    .show();
        }
    }
}
