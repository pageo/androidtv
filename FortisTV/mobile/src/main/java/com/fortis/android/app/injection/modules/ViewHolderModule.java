package com.fortis.android.app.injection.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.fortis.android.core.injection.qualifiers.ActivityContext;
import com.fortis.android.core.injection.scopes.PerViewHolder;
import com.fortis.android.core.ui.base.AppCompatLifecycleActivity;
import com.fortis.android.core.ui.base.navigator.ActivityNavigator;
import com.fortis.android.core.ui.base.navigator.INavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewHolderModule {
    // ----- Fields
    private final AppCompatLifecycleActivity activity;

    // ----- Constructors
    public ViewHolderModule(View itemView) {
        activity = (AppCompatLifecycleActivity) itemView.getContext();
    }

    // ----- Methods
    @Provides
    @PerViewHolder
    @ActivityContext
    Context provideActivityContext() { return activity; }

    @Provides
    @PerViewHolder
    FragmentManager provideFragmentManager() { return activity.getSupportFragmentManager(); }

    @Provides
    @PerViewHolder
    INavigator provideNavigator() { return new ActivityNavigator(activity); }
}
