package com.fortis.android.app.ui.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityWelcomeBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.main.MainActivity;
import com.fortis.android.core.ui.base.navigator.INavigator;
import com.fortis.android.core.ui.base.viewModels.NoOpViewModel;
import com.fortis.android.core.util.ToastPresenter;
import com.tumblr.remember.Remember;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity<ActivityWelcomeBinding, NoOpViewModel> {

    // ----- Const
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;

    // ----- Fields
    @BindView(R.id.mSplashPhoto)
    ImageView mSplashPhoto;
    @BindView(R.id.mLoginBut)
    Button mLoginBut;

    @Inject
    ToastPresenter toastPresenter;

    @Inject
    INavigator navigator;

    // ----- Override methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_welcome, savedInstanceState);

        checkToken();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            if (resultCode != RESULT_OK) {
                toastPresenter.showShortToast("Draw over other app permission not available. Closing the application");
                finish();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    // ----- Private methods
    private void checkToken() {
        String token = Remember.getString("Token", "");
        if (TextUtils.isEmpty(token)) {
            mLoginBut.setVisibility(View.VISIBLE);
            return;
        }
        showSplashPhoto();
    }

    private void showSplashPhoto() {
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(mSplashPhoto, "alpha", 0, 1);
        alphaAnim.setDuration(3000);
        alphaAnim.start();
        alphaAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                navigator.startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                navigator.finishActivity();
            }
        });
    }

    // ----- Callback methods
    @OnClick(R.id.mLoginBut)
    void onLoginButtonClick() {
        navigator.startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        navigator.finishActivity();
    }

    @OnClick(R.id.btnLinkToFloatWidget)
    void onLinkToFloatWidgetButtonClick() {
        //appNavigator.gotoFloatWidget();
    }
}
