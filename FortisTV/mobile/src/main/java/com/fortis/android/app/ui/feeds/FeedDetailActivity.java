package com.fortis.android.app.ui.feeds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityFeedDetailBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.core.ui.base.navigator.INavigator;
import com.fortis.android.core.viewmodel.FeedDetailViewModel;

import org.parceler.Parcels;

import butterknife.BindView;

public class FeedDetailActivity extends BaseActivity<ActivityFeedDetailBinding, FeedDetailViewModel> {

    // ----- Fields
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.author)
    TextView author;

    // ----- Override methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_feed_detail, savedInstanceState);
        viewModel.update(Parcels.unwrap(getIntent().getParcelableExtra(INavigator.EXTRA_ARGS)), false);
    }
}
