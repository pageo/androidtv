package com.fortis.android.app.injection.components;

import com.fortis.android.app.ui.feeds.FeedsFragment;
import com.fortis.android.app.ui.shows.movies.MoviesFragment;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.modules.FragmentModule;
import com.fortis.android.core.injection.modules.ProviderModule;
import com.fortis.android.core.injection.scopes.PerFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {FragmentModule.class, ProviderModule.class})
public interface FragmentComponent {
    void inject(FeedsFragment fragment);
    void inject(MoviesFragment fragment);
}
