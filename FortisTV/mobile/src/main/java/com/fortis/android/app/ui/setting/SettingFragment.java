package com.fortis.android.app.ui.setting;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.fortis.android.app.R;
import com.fortis.android.core.viewmodel.SettingViewModel;
import com.fortis.android.core.util.AppUtils;

public class SettingFragment extends PreferenceFragmentCompat {

    // ----- Fields
    private SettingViewModel mSettingViewModel;

    // ----- Override methods
    @SuppressWarnings("deprecation")
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.setting, rootKey);
        mSettingViewModel = new SettingViewModel();
        findPreference(mSettingViewModel.getAboutTitle())
                .setOnPreferenceClickListener(
                        preference -> {
                            TextView msg = new TextView(preference.getContext());
                            msg.setText(Html.fromHtml(mSettingViewModel.getSummary()));
                            msg.setMovementMethod(LinkMovementMethod.getInstance());
                            msg.setClickable(true);

                            new AlertDialog.Builder(getActivity())
                                    .setTitle(mSettingViewModel.getAboutTitle())
                                    .setView(msg)
                                    .setPositiveButton("ok", null)
                                    .show();
                            return false;
                        });
        findPreference(mSettingViewModel.getVersionTitle())
                .setSummary(AppUtils.getVersion(getActivity()));

    }
}
