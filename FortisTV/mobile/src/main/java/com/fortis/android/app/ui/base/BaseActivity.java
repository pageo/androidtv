package com.fortis.android.app.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.fortis.android.app.BR;
import com.fortis.android.app.injection.components.ActivityComponent;
import com.fortis.android.app.injection.components.DaggerActivityComponent;
import com.fortis.android.core.MainApplication;
import com.fortis.android.core.injection.modules.ActivityModule;
import com.fortis.android.core.ui.base.AppCompatLifecycleActivity;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.realm.Realm;

@SuppressWarnings("rawtypes")
public abstract class BaseActivity<B extends ViewDataBinding, V extends BaseViewModel> extends AppCompatLifecycleActivity {

    // ----- Fields
    protected B binding;

    @Inject
    protected V viewModel;

    @Inject
    Realm realm;

    private ActivityComponent mActivityComponent;

    // ----- Methods
    protected final void setAndBindContentView(@LayoutRes int layoutResId, @Nullable Bundle savedInstanceState) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must not be null and should be injected via activityComponent().inject(this)"); }
        binding = DataBindingUtil.setContentView(this, layoutResId);
        binding.setVariable(BR.vm, viewModel);
        ButterKnife.bind(this);
    }

    protected final ActivityComponent activityComponent() {
        if(mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(MainApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        super.onDestroy();
        if(realm != null) { realm.close(); }
        binding = null;
        viewModel = null;
        mActivityComponent = null;
        realm = null;
    }
}
