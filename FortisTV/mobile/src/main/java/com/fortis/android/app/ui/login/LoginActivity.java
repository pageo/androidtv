package com.fortis.android.app.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ProgressBar;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityWelcomeBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.main.MainActivity;
import com.fortis.android.app.ui.register.RegisterActivity;
import com.fortis.android.core.ui.base.navigator.INavigator;
import com.fortis.android.core.util.ToastPresenter;
import com.fortis.android.core.viewmodel.LoginViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class LoginActivity extends BaseActivity<ActivityWelcomeBinding, LoginViewModel> {

    // ----- Fields
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    INavigator navigator;

    @Inject
    ToastPresenter toastPresenter;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    // ----- Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_login, savedInstanceState);
    }

    // ----- Callback methods
    @OnClick(R.id.btnLogin)
    void onLoginButtonClick() {
        if (!viewModel.getEmail().isEmpty() && !viewModel.getPassword().isEmpty())
            checkLogin();
        else
            loginFailure();
    }

    @OnClick(R.id.btnLinkToRegisterScreen)
    void onLinkToRegisterClick() {
        navigator.startActivity(new Intent(this, RegisterActivity.class));
        navigator.finishActivity();
    }

    // ----- Internal logics
    private void checkLogin() {
        compositeDisposable.clear();
        compositeDisposable.add(viewModel.authenticate()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(token -> {
                            if (!TextUtils.isEmpty(token))
                                loginSuccess();
                        },
                        throwable -> loginFailure()));
    }

    private void loginSuccess() {
        navigator.startActivity(new Intent(this, MainActivity.class));
        navigator.finishActivity();
    }

    private void loginFailure() {
        toastPresenter.showShortToast("sign in failed , please try again .");
    }
}
