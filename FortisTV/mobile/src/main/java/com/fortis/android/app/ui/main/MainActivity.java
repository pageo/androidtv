package com.fortis.android.app.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityFeedDetailBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.app.ui.feeds.FeedsFragment;
import com.fortis.android.app.ui.login.WelcomeActivity;
import com.fortis.android.app.ui.setting.SettingFragment;
import com.fortis.android.app.ui.shows.movies.MoviesFragment;
import com.fortis.android.core.data.model.User;
import com.fortis.android.core.ui.base.navigator.INavigator;
import com.fortis.android.core.viewmodel.MainViewModel;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.tumblr.remember.Remember;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class MainActivity extends BaseActivity<ActivityFeedDetailBinding, MainViewModel> {

    // ----- Fields
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private AccountHeader headerResult = null;
    private Drawer result = null;

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @Inject
    INavigator navigator;

    // ----- Override methods
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_main, savedInstanceState);
        drawerImageLoaderInit();
        setSupportActionBar(toolbar);
        setupDrawer(savedInstanceState);
        if (savedInstanceState == null)
            navigator.replaceFragment(R.id.content, new FeedsFragment(), savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
    }

    @Override
    public void onPause() {
        unBind();
        super.onPause();
    }

    // ----- Internal logic
    private void setupDrawer(Bundle savedInstanceState) {

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(new ProfileDrawerItem().withIdentifier(1).withName("----"))
                .withSelectionListEnabledForSingleProfile(false)
                .withSavedInstance(savedInstanceState)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();
        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName("Shots").withIcon(GoogleMaterial.Icon.gmd_spa)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Feeds").withIcon(GoogleMaterial.Icon.gmd_feedback).withIdentifier(1).withSelectable(true)
                                ),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("Movies & TV").withIcon(GoogleMaterial.Icon.gmd_video_library)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Movies").withIcon(GoogleMaterial.Icon.gmd_video_label).withIdentifier(2).withSelectable(true),
                                        new SecondaryDrawerItem().withName("TV").withIcon(GoogleMaterial.Icon.gmd_tv).withIdentifier(3).withSelectable(true)
                                ),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("Settings").withIcon(GoogleMaterial.Icon.gmd_settings).withIdentifier(98).withSelectable(true)
                )
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName("Logout").withIcon(GoogleMaterial.Icon.gmd_cancel).withIdentifier(99).withSelectable(false),
                        new SecondaryDrawerItem().withName("Exit").withIcon(GoogleMaterial.Icon.gmd_exit_to_app).withIdentifier(100).withSelectable(false)
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    int identifier = (int) drawerItem.getIdentifier();
                    switch (identifier) {
                        case 1:
                            navigator.replaceFragment(R.id.content, new FeedsFragment(), savedInstanceState);
                            break;
                        case 2:
                            navigator.replaceFragment(R.id.content, new MoviesFragment(), savedInstanceState);
                            break;
                        case 98:
                            navigator.replaceFragment(R.id.content, new SettingFragment(), savedInstanceState);
                            break;
                        case 99:
                            Remember.clear();
                            navigator.startActivity(new Intent(this, WelcomeActivity.class));
                            navigator.finishActivity();
                            break;
                        case 100:
                            navigator.finishActivity();
                            System.exit(0);
                            break;
                    }
                    return false;
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }
    private void drawerImageLoaderInit() {
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                RequestOptions options = new RequestOptions()
                        .placeholder(placeholder);
                Glide.with(imageView.getContext())
                        .load(uri)
                        .apply(options)
                        .into(imageView);
            }
            @Override
            public void cancel(ImageView imageView) {
                Glide.with(imageView.getContext()).clear(imageView);
            }
            @Override
            public Drawable placeholder(Context ctx, String tag) {
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag))
                    return DrawerUIUtils.getPlaceHolder(ctx);
                else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag))
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.primary).sizeDp(56);
                else if ("customUrlItem".equals(tag))
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(com.fortis.android.core.R.color.md_red_500).sizeDp(56);
                return super.placeholder(ctx, tag);
            }
        });
    }
    private void bind() {
        compositeDisposable.add(viewModel.getUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> loadProfileUser(user),
                        throwable -> Timber.e(throwable, "Could not load user")));
    }

    private void unBind() {
        compositeDisposable.clear();
    }

    private void loadProfileUser(User user) {
        ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem()
                .withIdentifier(1)
                .withName(user.login);
        if (user.email != null)
            profileDrawerItem.withEmail(user.email);
        if (user.avatar != null)
            profileDrawerItem.withIcon(user.avatar);
        headerResult.updateProfile(profileDrawerItem);
    }
}
