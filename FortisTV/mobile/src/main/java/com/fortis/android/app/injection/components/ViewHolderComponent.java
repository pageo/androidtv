package com.fortis.android.app.injection.components;

import com.fortis.android.app.injection.modules.ViewHolderModule;
import com.fortis.android.app.ui.feeds.FeedViewHolder;
import com.fortis.android.app.ui.shows.movies.MovieViewHolder;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.scopes.PerViewHolder;

import dagger.Component;

@PerViewHolder
@Component(dependencies = ApplicationComponent.class, modules = {ViewHolderModule.class})
public interface ViewHolderComponent {
    void inject(FeedViewHolder viewHolder);
    void inject(MovieViewHolder viewHolder);
}
