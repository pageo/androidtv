package com.fortis.android.app.ui.feeds;

import android.view.View;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ItemFeedBinding;
import com.fortis.android.app.ui.base.BaseViewHolder;
import com.fortis.android.core.viewmodel.FeedViewModel;
import com.fortis.android.core.ui.base.navigator.INavigator;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.OnClick;

public class FeedViewHolder extends BaseViewHolder<ItemFeedBinding, FeedViewModel> {

    // ----- Fields
    @Inject
    INavigator navigator;

    // ----- Constructors
    public FeedViewHolder(View v) {
        super(v);
        viewHolderComponent().inject(this);
        bindContentView(v);
    }

    // ----- Callback methods
    @OnClick(R.id.card_view)
    void onItemClick(View v) {
        navigator.startFragmentActivity(FeedDetailActivity.class, Parcels.wrap(viewModel.getFeed()));
    }
}
