package com.fortis.android.app.ui.shows.movies;

import android.view.View;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ItemMovieBinding;
import com.fortis.android.app.ui.base.BaseViewHolder;
import com.fortis.android.core.viewmodel.MovieViewModel;
import com.fortis.android.core.ui.base.navigator.INavigator;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.OnClick;

public class MovieViewHolder extends BaseViewHolder<ItemMovieBinding, MovieViewModel> {

    // ----- Fields
    @Inject
    INavigator navigator;

    // ----- Constructors
    public MovieViewHolder(View v) {
        super(v);
        viewHolderComponent().inject(this);
        bindContentView(v);
    }

    // ----- Callback methods
    @OnClick(R.id.card_view)
    void onItemClick(View v) {
        navigator.startFragmentActivity(MovieDetailActivity.class, Parcels.wrap(viewModel.getMovie()));
    }
}
