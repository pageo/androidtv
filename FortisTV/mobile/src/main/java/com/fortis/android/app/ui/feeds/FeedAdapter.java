package com.fortis.android.app.ui.feeds;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortis.android.app.R;
import com.fortis.android.core.data.model.Feed;
import com.fortis.android.core.injection.scopes.PerFragment;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder> {

    // ----- Fields
    private List<Feed> feedList = Collections.emptyList();

    // ----- Constructors
    @Inject
    public FeedAdapter() {}

    // ----- Public methods
    public void setFeedList(List<Feed> feedList) {
        this.feedList = feedList;
    }

    // ----- Override methods
    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_feed, viewGroup, false);
        return new FeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder feedViewHolder, int position) {
        feedViewHolder.viewModel().update(feedList.get(position), position == feedList.size() - 1);
        feedViewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return feedList.size();
    }
}
