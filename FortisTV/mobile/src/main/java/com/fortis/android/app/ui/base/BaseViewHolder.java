package com.fortis.android.app.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fortis.android.app.BR;
import com.fortis.android.app.injection.components.DaggerViewHolderComponent;
import com.fortis.android.app.injection.components.ViewHolderComponent;
import com.fortis.android.app.injection.modules.ViewHolderModule;
import com.fortis.android.core.MainApplication;
import com.fortis.android.core.ui.base.viewModels.BaseViewModel;

import javax.inject.Inject;

import butterknife.ButterKnife;

@SuppressWarnings("rawtypes")
public abstract class BaseViewHolder<B extends ViewDataBinding, V extends BaseViewModel> extends RecyclerView.ViewHolder {

    // ----- Fields
    protected B binding;

    @Inject
    protected V viewModel;

    private ViewHolderComponent viewHolderComponent;

    private View itemView = null;

    // ----- Constructors
    public BaseViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    // ----- Methods
    protected final ViewHolderComponent viewHolderComponent() {
        if(viewHolderComponent == null) {
            viewHolderComponent = DaggerViewHolderComponent.builder()
                    .viewHolderModule(new ViewHolderModule(itemView))
                    .applicationComponent(MainApplication.get(itemView.getContext()).getComponent())
                    .build();
            itemView = null;
        }
        return viewHolderComponent;
    }

    protected final void bindContentView(@NonNull View view) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must not be null and should be injected via viewHolderComponent().inject(this)"); }
        binding = DataBindingUtil.bind(view);
        binding.setVariable(BR.vm, viewModel);
        ButterKnife.bind(this, view);
    }

    public final V viewModel() { return viewModel; }

    public final void executePendingBindings() {
        if(binding != null) { binding.executePendingBindings(); }
    }
}
