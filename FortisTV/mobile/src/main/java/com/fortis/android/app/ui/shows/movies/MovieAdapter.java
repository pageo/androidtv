package com.fortis.android.app.ui.shows.movies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortis.android.app.R;
import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.injection.scopes.PerFragment;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class MovieAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    // ----- Fields
    private List<Movie> movieList = Collections.emptyList();

    // ----- Constructors
    @Inject
    public MovieAdapter() {
    }

    // ----- Public methods
    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }

    // ----- Override methods
    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_movie, viewGroup, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int position) {
        movieViewHolder.viewModel().update(movieList.get(position));
        movieViewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
