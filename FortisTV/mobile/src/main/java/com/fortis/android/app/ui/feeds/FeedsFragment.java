package com.fortis.android.app.ui.feeds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortis.android.app.R;
import com.fortis.android.app.databinding.FragmentRecyclerviewBinding;
import com.fortis.android.app.ui.base.BaseFragment;
import com.fortis.android.core.viewmodel.FeedsViewModel;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class FeedsFragment extends BaseFragment<FragmentRecyclerviewBinding, FeedsViewModel> {

    // ----- Fields
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    FeedAdapter adapter;

    // ----- Override methods
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, R.layout.fragment_recyclerview, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
        binding.swipeRefreshLayout.setOnRefreshListener(() -> reloadData());
        if (savedInstanceState == null)
            binding.swipeRefreshLayout.post(() -> binding.swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
    }

    @Override
    public void onPause() {
        unBind();
        super.onPause();
    }

    // ----- Internal logics
    public void onRefresh(boolean... params) {
        binding.swipeRefreshLayout.setRefreshing(params[0]);
        if (params.length >= 2 && params[1]) {
            Snackbar.make(binding.recyclerView, "Could not load feeds", Snackbar.LENGTH_SHORT)
                    .setAction(R.string.snackbar_action_retry, v -> reloadData())
                    .show();
        }
    }
    private void bind() {
        mCompositeDisposable.add(viewModel.getFeeds()
                .doOnSuccess(Collections::sort)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(feeds -> {
                    adapter.setFeedList(feeds);
                    adapter.notifyDataSetChanged();
                    onRefresh(false);
                }, throwable -> {
                    Timber.e(throwable, "Could not load feeds");
                    onRefresh(false, true);
                }));
    }

    private void unBind() {
        mCompositeDisposable.clear();
    }

    private void reloadData(){
        unBind();
        bind();
    }
}
