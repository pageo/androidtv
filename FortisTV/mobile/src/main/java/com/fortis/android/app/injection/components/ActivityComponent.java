package com.fortis.android.app.injection.components;

import com.fortis.android.app.ui.feeds.FeedDetailActivity;
import com.fortis.android.app.ui.login.LoginActivity;
import com.fortis.android.app.ui.login.WelcomeActivity;
import com.fortis.android.app.ui.main.MainActivity;
import com.fortis.android.app.ui.register.RegisterActivity;
import com.fortis.android.app.ui.shows.movies.MovieDetailActivity;
import com.fortis.android.core.injection.components.ApplicationComponent;
import com.fortis.android.core.injection.modules.ActivityModule;
import com.fortis.android.core.injection.modules.ProviderModule;
import com.fortis.android.core.injection.scopes.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ProviderModule.class})
public interface ActivityComponent {
    void inject(MainActivity activity);
    void inject(WelcomeActivity activity);
    void inject(LoginActivity activity);
    void inject(RegisterActivity activity);
    void inject(FeedDetailActivity activity);
    void inject(MovieDetailActivity activity);
}
