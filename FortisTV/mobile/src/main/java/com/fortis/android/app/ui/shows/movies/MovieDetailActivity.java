package com.fortis.android.app.ui.shows.movies;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fortis.android.app.R;
import com.fortis.android.app.databinding.ActivityMovieDetailBinding;
import com.fortis.android.app.ui.base.BaseActivity;
import com.fortis.android.core.viewmodel.MovieDetailViewModel;
import com.fortis.android.core.data.model.Movie;
import com.fortis.android.core.ui.base.navigator.INavigator;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MovieDetailActivity extends BaseActivity<ActivityMovieDetailBinding, MovieDetailViewModel> {

    // ----- Fields
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Nullable
    @BindView(R.id.image_dialog_image_view)
    ImageView imageDialogImageView;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;

    // ----- Override methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(R.layout.activity_movie_detail, savedInstanceState);
        viewModel.update(Parcels.unwrap(getIntent().getParcelableExtra(INavigator.EXTRA_ARGS)));
    }

    // ----- Callbacks
    @Optional
    @OnClick(R.id.image_dialog_image_view)
    public void onImageDialogClick() {
        relativeLayout.removeView(imageDialogImageView);
    }

    @OnClick(R.id.movie_image_view)
    public void onPosterClick() {
        View view = getLayoutInflater().inflate(R.layout.full_width_image_view, relativeLayout);
        ButterKnife.bind(this, view);

        RequestOptions options = new RequestOptions()
                .fitCenter()
                .placeholder(R.color.placeholder);
        Glide.with(this)
                .load(viewModel.getPosterPath())
                .apply(options)
                .into(imageDialogImageView);
    }

    @OnClick(R.id.movie_alternate_title_title_view)
    public void onTitleClick(View view) {
        compositeDisposable.clear();
        compositeDisposable.add(viewModel.getTheMovie()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movie -> showSourcesDialog(movie), throwable -> {
                    Timber.e(throwable, "Could not load movie");
                }));
    }

    // ----- Private methods
    public void postIntent (String videoUrl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(videoUrl), "video/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @SuppressWarnings("deprecation")
    public void showSourcesDialog(Movie movie) {
        if (movie.getVideos().size() >= 1) {
            new MaterialDialog.Builder(this)
                    .title(getString(R.string.sources))
                    .itemsCallbackSingleChoice(0, (materialDialog, view, i, charSequence) -> true)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) { //stream
                            super.onPositive(dialog);
                            postIntent(movie.getVideos().get(0).getUrl());
                        }
                        @Override
                        public void onNeutral(MaterialDialog dialog) { // cancel
                            super.onNeutral(dialog);
                        }
                    })
                    .widgetColorRes(R.color.md_deep_orange_800)
                    .positiveText(R.string.stream)
                    .positiveColorRes(R.color.md_deep_orange_800)
                    .neutralText(R.string.cancel)
                    .neutralColorRes(R.color.grey_darkestXX)
                    .cancelable(true)
                    .show();
        } else {
            Timber.e(new Throwable("No sources found."));
        }
    }
}
